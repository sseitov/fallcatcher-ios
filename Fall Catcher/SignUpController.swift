//
//  SignUpController.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 13.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import SVProgressHUD

class SignUpController: UIViewController, TextFieldContainerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, PolicyControllerDelegate {
    
    @IBOutlet weak var avatarButton: UIButton!
    @IBOutlet weak var roleButton: UIButton!
    @IBOutlet weak var number: TextFieldContainer!
    @IBOutlet weak var name: TextFieldContainer!
    @IBOutlet weak var email: TextFieldContainer!
    @IBOutlet weak var password: TextFieldContainer!
    @IBOutlet weak var phone: TextFieldContainer!
    @IBOutlet weak var code: TextFieldContainer!
    @IBOutlet weak var codeHeightConstraint: NSLayoutConstraint!
    
    private var avatar:UIImage?
    private var sentCode:Int?
    private var phoneNum:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tap))
        self.view.addGestureRecognizer(tap)

        number.activeColor = UIColor.clear
        number.textFontColor = UIColor.white
        number.textType = .numbersAndPunctuation
        number.placeholder = "Social Security Number"
        number.returnType = .done
        number.delegate = self

        name.activeColor = UIColor.clear
        name.textFontColor = UIColor.white
        name.autocapitalizationType = .words
        name.placeholder = "Full Name"
        name.returnType = .done
        name.delegate = self
        
        email.activeColor = UIColor.clear
        email.textFontColor = UIColor.white
        email.textType = .emailAddress
        email.placeholder = "E-Mail"
        email.returnType = .done
        email.delegate = self
        
        password.activeColor = UIColor.clear
        password.textFontColor = UIColor.white
        password.placeholder = "Password"
        password.returnType = .done
        password.secure = true
        password.delegate = self
        
        phone.activeColor = UIColor.clear
        phone.textFontColor = UIColor.white
        phone.textType = .phonePad
        phone.placeholder = "(000) 000-0000"
        phone.returnType = .done
        phone.delegate = self
        
        code.textType = .numbersAndPunctuation
        code.nonActiveColor = UIColor.white
        code.placeholderColor = UIColor.lightGray
        code.placeholder = "Verification Code from SMS"
        code.returnType = .go
        code.delegate = self
        code.alpha = 0
        
        codeHeightConstraint.constant = 0
    }
    
    @objc func tap() {
        TextFieldContainer.deactivateAll()
    }

    @IBAction func back(_ sender: Any) {
        goBack()
    }
    
    // MARK: - Phone number verification
  
    func confirmPhone(_ number:String) {
        let phoneNumber = "+1\(number)"
        sentCode = Int.random(1000...9999)
        
        SVProgressHUD.show(withStatus: "Verify...")
        Twilio.shared.sendSMS("Your Fall Catcher verification code is: \(sentCode!)", to: phoneNumber, success: { error in
            SVProgressHUD.dismiss()
            if error == nil {
                self.showMessage("We have sent SMS message to your phone number. Please check it and enter code below.",
                                 messageType: .information, messageHandler:
                    {
                        self.codeHeightConstraint.constant = 36
                        UIView.animate(withDuration: 0.5, animations: {
                            self.view.layoutIfNeeded()
                            self.code.alpha = 1
                        }, completion: { _ in
                            self.code.activate(true)
                        })
                })
            } else {
                self.showMessage(error!.localizedDescription, messageType: .error)
            }
        })
    }
    
    // MARK: - TextFieldContainer Delegate
    
    func textDone(_ sender:TextFieldContainer, text:String?) {
        if sender == number {
            if number.text() != number.text().digitsFromString() {
                showMessage("Field can contains only digits.", messageType: .error, messageHandler: {
                    self.number.activate(true)
                })
            } else if number.text().length() < 3 || number.text().length() > 10 {
                showMessage("Field length must be from 3 to 10 digits.", messageType: .error, messageHandler: {
                    self.number.activate(true)
                })
            }
        } else if sender == name {
            if name.text().isEmpty {
                showMessage("Name field can not be empty.", messageType: .error, messageHandler: {
                    self.name.activate(true)
                })
            }
        } else if sender == email {
            if !email.text().isEmail() {
                showMessage("Email address should have xxxx@domain.prefix format.", messageType: .error, messageHandler: {
                    self.email.activate(true)
                })
            }
        } else if sender == password {
            if password.text().length() < 8 {
                showMessage("Password must contain 8 or more characters.", messageType: .error, messageHandler: {
                    self.password.activate(true)
                })
            }
        } else if sender == code {
            if sentCode != nil && sentCode! == Int(sender.text()) {
                if checkProfile() {
                    signUp()
                }
            } else {
                showMessage("Invalid confirmation code.", messageType: .error)
            }
        }
    }

    func textStartEditing(_ sender:TextFieldContainer) {
        if (sender == phone) {
            sender.setText("")
        }
    }
    
    func textEndEditing(_ sender:TextFieldContainer) {
        if (sender == phone) {
            if sender.text().normalizePhone().length() < 10 {
                sender.setText("")
            }
        }
    }

    func textChange(_ sender:TextFieldContainer, inRange: NSRange, string:String) -> Bool {
        if (sender == phone) {
            let newString = (sender.text() as NSString).replacingCharacters(in: inRange, with: string)
            let num = newString.normalizePhone()
            if num.length() > 10 {
                return false
            } else if num.length() == 10 {
                sender.setText(newString)
                sender.activate(false)
                phoneNum = num
                return false
            }
            if num.length() == 3 && string.length() > 0 {
                sender.setText("(\(num)) ")
                return false
            }
            if num.length() == 6 && string.length() > 0 {
                let index = num.index(num.startIndex, offsetBy: 3)
                let code = num[..<index]
                let num = num[index...]
                sender.setText("(\(code)) \(num)-")
                return false
            }
            return true
        } else {
            return true
        }
    }
    
    // MARK: - UIImagePickerController delegate
    
    @IBAction func inputAvatar(_ sender: UIButton) {
        UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder), to: nil, from: nil, for: nil)
        let actionView = ActionSheet.create(
            title: "Choose Photo",
            actions: ["From Camera Roll", "Use Camera"],
            handler1: {
                let imagePicker = UIImagePickerController()
                imagePicker.allowsEditing = false
                imagePicker.sourceType = .photoLibrary
                imagePicker.delegate = self
                imagePicker.modalPresentationStyle = .formSheet
                if let font = UIFont(name: "HelveticaNeue-CondensedBold", size: 15) {
                    imagePicker.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.mainColor(), NSAttributedStringKey.font : font]
                }
                imagePicker.navigationBar.tintColor = UIColor.mainColor()
                self.present(imagePicker, animated: true, completion: nil)
        }, handler2: {
            let imagePicker = UIImagePickerController()
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        })
        actionView?.show()
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: {
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                self.avatar = pickedImage.withSize(CGSize(width:300, height:300))
                self.avatarButton.setImage(self.avatar!.inCircle(), for: .normal)
            }
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Sign Up
    
    func checkProfile() -> Bool {
        if number.text() != number.text().digitsFromString() {
            showMessage("Field can contains only digits.", messageType: .error, messageHandler: {
                self.number.activate(true)
            })
            return false
        }
        if number.text().length() < 3 || number.text().length() > 10 {
            showMessage("Field length must be from 3 to 10 digits.", messageType: .error, messageHandler: {
                self.number.activate(true)
            })
            return false
        }
        if name.text().isEmpty {
            let ask = createQuestion("You must provide your full name for continue registration. Otherwise you can go back to login screen and login as guest.", acceptTitle: "Provide", cancelTitle: "Go back", acceptHandler:{
                self.name.activate(true)
            }, cancelHandler: {
                self.goBack()
            })
            ask?.show()
            return false
        }
        if !email.text().isEmail() {
            let ask = createQuestion("You must provide your email for continue registration. Email address should have xxxx@domain.prefix format. Otherwise you can go back to login screen and login as guest.", acceptTitle: "Provide", cancelTitle: "Go back", acceptHandler:{
                self.email.activate(true)
            }, cancelHandler: {
                self.goBack()
            })
            ask?.show()

            return false
        }
        if password.text().length() < 8 {
            let ask = createQuestion("You must enter your password for continue registration. Password must contain 8 or more characters. Otherwise you can go back to login screen and login as guest.", acceptTitle: "Enter", cancelTitle: "Go back", acceptHandler:{
                self.password.activate(true)
            }, cancelHandler: {
                self.goBack()
            })
            ask?.show()

            return false
        }
        if phoneNum == nil || phoneNum!.isEmpty {
            let ask = createQuestion("You must provide your phone number for continue registration.Otherwise you can go back to login screen and login as guest.", acceptTitle: "Provide", cancelTitle: "Go back", acceptHandler:{
                self.password.activate(true)
            }, cancelHandler: {
                self.goBack()
            })
            ask?.show()
            return false
        }

        return true
    }
    
    @IBAction func doSignUp(_ sender: Any) {
        if checkProfile() {
            if avatar == nil {
                let ask = createQuestion("Do you want to setup your profile image?", acceptTitle: "Setup", cancelTitle: "Skip", acceptHandler: {
                    self.inputAvatar(self.avatarButton)
                }, cancelHandler: {
                    self.performSegue(withIdentifier: "acceptPolicy", sender: nil)
                })
                ask?.show()
            } else {
                self.performSegue(withIdentifier: "acceptPolicy", sender: nil)
            }
        }
    }
    
    func acceptPolicy() {
        dismiss(animated: true, completion: {
            self.codeHeightConstraint.constant = 36
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutIfNeeded()
            }, completion: { _ in
                self.confirmPhone(self.phoneNum!)
            })
        })
    }

    func signUp() {
        SVProgressHUD.show(withStatus: "SignUp...")
        let role = self.roleButton.title(for: .normal)
        FallCatcherAPI.shared.signUp(name: name.text(),
                              email: email.text(),
                              password: password.text(),
                              phone: phone.text().normalizePhone(),
                              role: role!,
                              number: number.text(),
                              avatar: avatar,
                              error:
            { err in
                SVProgressHUD.dismiss()
                if err != nil {
                    self.showMessage(err!.localizedDescription, messageType: .error, messageHandler: {
                        self.codeHeightConstraint.constant = 0
                        UIView.animate(withDuration: 0.5, animations: {
                            self.view.layoutIfNeeded()
                            self.code.alpha = 0
                        }, completion: { _ in
                            self.code.setText("")
                        })
                    })
                } else {
                    AppUnlocked = true
                    self.navigationController?.performSegue(withIdentifier: "unwindToMenu", sender: self)
                }
        })
    }
    
    @IBAction func changeRole(_ sender: UIButton) {
        let alert = RolePicker.create({ role in
            sender.setTitle(role, for: .normal)

            if role == "Patient" {
                self.number.placeholder = "Social Security Number"
            } else {
                self.number.placeholder = "National Provider Number"
            }
            self.number.setText("")
        })
        alert?.show()
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "acceptPolicy" {
            let next = segue.destination as! PolicyController
            next.delegate = self
        }
    }

}
