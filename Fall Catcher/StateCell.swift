//
//  StateCell.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 21.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit

class StateCell: UITableViewCell {

    @IBOutlet weak var fieldName: UILabel!
    @IBOutlet weak var fieldValue: UILabel!
    
    var field:String?
    var value:String? {
        didSet {
            if value != nil {
                fieldValue.textColor = UIColor.black
                fieldValue.text = value
            } else {
                fieldValue.textColor = UIColor.lightGray
                fieldValue.text = "Select State"
            }
        }
    }
    
    var delegate:CellDelegate? {
        didSet {
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
            fieldValue.addGestureRecognizer(tap)
        }
    }
    
    @objc func didTap() {
        let alert = StatePicker.create({ code, state in
            if state != nil {
                self.value = state
                self.delegate?.fieldDidChange(self.field!, value: state)
            } else {
                self.value = nil
                self.delegate?.fieldDidChange(self.field!, value: nil)
            }
        })
        alert?.show()
    }

}
