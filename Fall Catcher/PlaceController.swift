//
//  PlaceController.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 20.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import CoreTelephony
import MessageUI

class PlaceController: UITableViewController, MFMailComposeViewControllerDelegate {

    var place:Place?
    private var address = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTitle(place!.Name()!)
        setupBackButton()
        
        if let addr = place!.Address() {
            address = addr
        }
        if let city = place!.City() {
            if address.isEmpty {
                address = city
            } else {
                address += ", \(city)"
            }
        }
        if let state = place!.State() {
            if address.isEmpty {
                address = state
            } else {
                address += ", \(state)"
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 80
        } else {
            return 44
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: nil)
        cell.selectionStyle = .none
        cell.detailTextLabel?.numberOfLines = 0
        cell.detailTextLabel?.lineBreakMode = .byWordWrapping
        cell.accessoryType = .none
        cell.textLabel?.font = UIFont.condensedFont(15)
        cell.detailTextLabel?.font = UIFont.mainFont()
        switch indexPath.row {
        case 0:
            cell.textLabel?.text = "Address"
            cell.detailTextLabel?.text = address
        case 1:
            cell.textLabel?.text = "Contact"
            cell.detailTextLabel?.text = place!.Contact()
        case 2:
            cell.imageView?.image =  UIImage(named: "email")
            if let email = place!.Email(), !email.isEmpty {
                cell.detailTextLabel?.text = "Send mail"
                cell.accessoryType = .disclosureIndicator
            }
        case 3:
            cell.imageView?.image =  UIImage(named: "call")
            if let phone = place?.Main_Phone(), !phone.isEmpty {
                cell.detailTextLabel?.text = phone.phoneText()
                cell.accessoryType = .disclosureIndicator
            }
        default:
            cell.imageView?.image = UIImage(named: "public")
            if let web = place?.Website(), let _ = URL(string: web) {
                cell.detailTextLabel?.text = "Web Site"
                cell.accessoryType = .disclosureIndicator
            }
        }

        return cell
    }
    
    private func canMakePhoneCall() -> Bool {
        guard let url = URL(string: "tel://") else {
            return false
        }
        
        let mobileNetworkCode = CTTelephonyNetworkInfo().subscriberCellularProvider?.mobileNetworkCode
        
        let isInvalidNetworkCode = mobileNetworkCode == nil
            || mobileNetworkCode!.count == 0
            || mobileNetworkCode == "65535"
        
        return UIApplication.shared.canOpenURL(url) && !isInvalidNetworkCode
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 4:
            if let web = place?.Website(), let url = URL(string: web) {
                performSegue(withIdentifier: "showWeb", sender: url)
            }
        case 3:
            if let phone = place?.Main_Phone() {
                if canMakePhoneCall() {
                    let num = "tel://+1\(phone.normalizePhone())"
                    if let url = URL(string: num) {
                        UIApplication.shared.openURL(url)
                    }
                } else {
                    self.showMessage("Your device can not make phone call.", messageType: .error)
                }
            }
        case 2:
            if let email = place!.Email(), !email.isEmpty {
                sendMail(email)
            }
        default:
            break
        }
    }
    
    fileprivate func sendMail(_ email:String) {
        if MFMailComposeViewController.canSendMail() {
            let mailController = MFMailComposeViewController()
            mailController.mailComposeDelegate = self
            mailController.setToRecipients([email])
            mailController.setSubject("Support")
            mailController.navigationBar.tintColor = UIColor.mainColor()
            if let font = UIFont(name: "HelveticaNeue-CondensedBold", size: 15) {
                mailController.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.mainColor(), NSAttributedStringKey.font : font]
            }
            present(mailController, animated: true, completion: nil)
        } else {
            showMessage("Your device could not send e-mail. Please check e-mail configuration and try again.", messageType: .error)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showWeb" {
            let next = segue.destination as! WebController
            next.placeName = place!.Name()!
            next.placeWebSite = sender as? URL
        }
    }

}
