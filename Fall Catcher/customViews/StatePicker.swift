//
//  StatePicker.swift
//  ispingle
//
//  Created by Сергей Сейтов on 15.03.17.
//  Copyright © 2016 ispingle. All rights reserved.
//

import UIKit

typealias StateCompletionBlock = (String?, String?) -> Void

fileprivate var states:[String] = []

class StatePicker: LGAlertView, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var statePickerView: UIPickerView!
    let dict = NSDictionary(contentsOf: Bundle.main.url(forResource: "us_states", withExtension: "plist")!)

    class func create(_ acceptHandler: @escaping StateCompletionBlock) -> StatePicker? {
        if let pickerAlert = Bundle.main.loadNibNamed("StatePicker", owner: nil, options: nil)?.first as? StatePicker {
            if let vals = pickerAlert.dict!.allValues as? [String] {
                states = vals.sorted()
            }
            pickerAlert.cancelButtonBlock = { alser in
                pickerAlert.dismiss()
                acceptHandler(nil, nil)
            }
            pickerAlert.otherButtonBlock = { alert in
                pickerAlert.dismiss()
                let val = states[pickerAlert.statePickerView.selectedRow(inComponent: 0)]
                let key = pickerAlert.dict!.allKeys(for: val)
                acceptHandler(key.first as? String, val)
            }
            pickerAlert.cancelButton.setupBorder(UIColor.clear, radius: 10)
            pickerAlert.otherButton.setupBorder(UIColor.clear, radius: 10)
            pickerAlert.containerView.setupBorder(UIColor.clear, radius: 15)
            return pickerAlert
        } else {
            return nil
        }
    }
    
    func showInView(_ view:UIView) {
        superView = view
        show()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return states.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return states[row]
    }
}
