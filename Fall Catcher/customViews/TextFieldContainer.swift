//
//  TextFieldContainer.swift
//  ispingle
//
//  Created by Сергей Сейтов on 15.03.17.
//  Copyright © 2017 ispingle. All rights reserved.
//

import UIKit

@objc protocol TextFieldContainerDelegate {
    func textDone(_ sender:TextFieldContainer, text:String?)
    func textChange(_ sender:TextFieldContainer, inRange: NSRange, string:String) -> Bool
    @objc optional func textStartEditing(_ sender:TextFieldContainer)
    @objc optional func textEndEditing(_ sender:TextFieldContainer)
}

class TextFieldContainer: UIView, UITextFieldDelegate {

    var delegate:TextFieldContainerDelegate?
    
    var nonActiveColor:UIColor = UIColor.clear
    var activeColor:UIColor = UIColor.clear
    var placeholderColor = UIColor.white
    
    var alignment:NSTextAlignment = .center {
        didSet {
            textField.textAlignment = alignment
        }
    }
    
    var textType:UIKeyboardType! {
        didSet {
            textField.keyboardType = textType
        }
    }
    var secure:Bool = false {
        didSet {
            textField.isSecureTextEntry = secure
        }
    }
    var returnType:UIReturnKeyType = .default {
        didSet {
            textField.returnKeyType = returnType
        }
    }
    var autocapitalizationType:UITextAutocapitalizationType = .none {
        didSet {
            textField.autocapitalizationType = autocapitalizationType
        }
    }
    
    var placeholder: String = "" {
        didSet {
            textField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedStringKey.foregroundColor : placeholderColor])
        }
    }
    
    var textFont: UIFont? = UIFont.mainFont(15) {
        didSet {
            textField.font = textFont
        }
    }
    
    var textFontColor: UIColor? = UIColor.black {
        didSet {
            textField.textColor = textFontColor
        }
    }
    
    var textField:UITextField!
    
    class func deactivateAll() {
        UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder), to: nil, from: nil, for: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField = UITextField(frame: self.bounds.insetBy(dx: 10, dy: 3))
        textField.textAlignment = .center
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        textField.delegate = self
        
        textField.font = textFont
        textField.textColor = UIColor.black
        
        self.addSubview(textField)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        textField = UITextField(frame: self.bounds.insetBy(dx: 10, dy: 3))
        textField.textAlignment = .center
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        textField.delegate = self
        
        backgroundColor = nonActiveColor
        setupBorder(UIColor.mainColor(), radius: 5)
        textField.font = textFont
        textField.textColor = textFontColor
        
        self.addSubview(textField)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        textField.frame = self.bounds.insetBy(dx: 10, dy: 3)
    }
    
    func activate(_ active:Bool) {
        backgroundColor = text().isEmpty ? nonActiveColor : activeColor
        if active {
            textField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
    }
    
    func text() -> String {
        return textField.text == nil ? "" : textField.text!
    }
    
    func setText(_ text:String) {
        textField.text = text
        backgroundColor = text.isEmpty ? nonActiveColor : activeColor
    }
    
    func clear() {
        textField.text = ""
        backgroundColor = nonActiveColor
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if delegate != nil && delegate!.textStartEditing != nil {
            delegate!.textStartEditing!(self)
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if delegate != nil && delegate!.textEndEditing != nil {
            delegate!.textEndEditing!(self)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText:String? = textField.text != nil ? (textField.text! as NSString).replacingCharacters(in: range, with: string) : nil
        if newText == nil || newText!.isEmpty {
            backgroundColor = nonActiveColor
        } else {
            backgroundColor = activeColor
        }
        if string == "\n" {
            textField.resignFirstResponder()
            delegate?.textDone(self, text: textField.text)
            return false
        } else {
            if delegate != nil {
                return delegate!.textChange(self, inRange: range, string: string)
            } else {
                return true
            }
        }
    }

}
