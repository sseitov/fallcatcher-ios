//
//  RolePicker.swift
//  ispingle
//
//  Created by Сергей Сейтов on 15.03.17.
//  Copyright © 2016 ispingle. All rights reserved.
//

import UIKit

typealias RoleCompletionBlock = (String) -> Void

fileprivate let roles = ["Patient", "Doctor", "Therapist", "Behavioral Health Technician", "Clinic Director"]

class RolePicker: LGAlertView, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var rolePickerView: UIPickerView!

    class func create(_ acceptHandler: @escaping RoleCompletionBlock) -> RolePicker? {
        if let pickerAlert = Bundle.main.loadNibNamed("RolePicker", owner: nil, options: nil)?.first as? RolePicker {
            pickerAlert.cancelButtonBlock = { alser in
                pickerAlert.dismiss()
                acceptHandler("Patient")
            }
            pickerAlert.otherButtonBlock = { alert in
                pickerAlert.dismiss()
                let val = roles[pickerAlert.rolePickerView.selectedRow(inComponent: 0)]
                acceptHandler(val)
            }
            pickerAlert.cancelButton.setupBorder(UIColor.clear, radius: 10)
            pickerAlert.otherButton.setupBorder(UIColor.clear, radius: 10)
            pickerAlert.containerView.setupBorder(UIColor.clear, radius: 15)
            return pickerAlert
        } else {
            return nil
        }
    }
    
    func showInView(_ view:UIView) {
        superView = view
        show()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return roles.count
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return roles[row]
    }
}
