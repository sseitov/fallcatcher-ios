//
//  DatePicker.swift
//  ispingle
//
//  Created by Сергей Сейтов on 15.03.17.
//  Copyright © 2016 ispingle. All rights reserved.
//

import UIKit

typealias DateCompletionBlock = (Date?) -> Void

class DatePicker: LGAlertView {

    @IBOutlet weak var datePickerView: UIDatePicker!
    
    class func create(initDate:Date?, acceptHandler: @escaping DateCompletionBlock, cancelHandler:LGAlertViewCancelBlock? = nil) -> DatePicker? {
        if let pickerAlert = Bundle.main.loadNibNamed("DatePicker", owner: nil, options: nil)?.first as? DatePicker {
            pickerAlert.cancelButtonBlock = cancelHandler
            pickerAlert.otherButtonBlock = { alert in
                pickerAlert.dismiss()
                acceptHandler(pickerAlert.datePickerView.date)
            }
            if initDate != nil {
                pickerAlert.datePickerView.date = initDate!
            }
            pickerAlert.cancelButton.setupBorder(UIColor.clear, radius: 10)
            pickerAlert.otherButton.setupBorder(UIColor.clear, radius: 10)
            pickerAlert.containerView.setupBorder(UIColor.clear, radius: 15)
            return pickerAlert
        } else {
            return nil
        }
    }
    
    func showInView(_ view:UIView) {
        superView = view
        show()
    }
}
