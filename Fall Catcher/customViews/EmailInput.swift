//
//  EmailInput.swift
//  SimpleVOIP
//
//  Created by Сергей Сейтов on 04.02.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit

typealias CompletionTextBlock = (String) -> Void

class EmailInput: LGAlertView, TextFieldContainerDelegate {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var inputField: TextFieldContainer!
    
    var handler:CompletionTextBlock?
    var checkEmail:Bool = false
    
    class func getEmail(cancelHandler:CompletionBlock?, acceptHandler:CompletionTextBlock?) -> EmailInput? {
        if let textInput = Bundle.main.loadNibNamed("EmailInput", owner: nil, options: nil)?.first as? EmailInput {
            textInput.inputField.delegate = textInput
            textInput.inputField.placeholder = "email"
            textInput.inputField.textType = .emailAddress
            textInput.cancelButtonBlock = { alert in
                cancelHandler!()
            }
            textInput.otherButtonBlock = { alert in
                if textInput.inputField.text().isEmail() {
                    textInput.dismiss()
                    acceptHandler!(textInput.inputField.text())
                } else {
                    textInput.showErrorMessage("Email should have xxxx@domain.prefix format.", animated: true)
                }
            }
            textInput.title.text = "Input your email"
            textInput.handler = acceptHandler
            textInput.checkEmail = true
            textInput.cancelButton.setupBorder(UIColor.clear, radius: 10)
            textInput.otherButton.setupBorder(UIColor.clear, radius: 10)
            textInput.containerView.setupBorder(UIColor.clear, radius: 15)

            UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder), to: nil, from: nil, for: nil)
            NotificationCenter.default.addObserver(textInput, selector: #selector(LGAlertView.keyboardWillChange(_:)), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
            
            return textInput
        } else {
            return nil
        }
    }
    
    class func getText(cancelHandler:CompletionBlock?, acceptHandler:CompletionTextBlock?) -> EmailInput? {
        if let textInput = Bundle.main.loadNibNamed("EmailInput", owner: nil, options: nil)?.first as? EmailInput {
            textInput.inputField.delegate = textInput
            textInput.inputField.placeholder = "city name"
            textInput.inputField.textType = .asciiCapable
            textInput.inputField.autocapitalizationType = .words
            textInput.cancelButtonBlock = { alert in
                cancelHandler!()
            }
            textInput.otherButtonBlock = { alert in
                textInput.dismiss()
                acceptHandler!(textInput.inputField.text())
            }
            textInput.title.text = "Input city name"
            textInput.handler = acceptHandler
            textInput.checkEmail = true

            UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder), to: nil, from: nil, for: nil)
            NotificationCenter.default.addObserver(textInput, selector: #selector(LGAlertView.keyboardWillChange(_:)), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
            
            return textInput
        } else {
            return nil
        }
    }

    func textDone(_ sender:TextFieldContainer, text:String?) {
        if checkEmail && !sender.text().isEmail() {
            showErrorMessage("Email should have xxxx@domain.prefix format.", animated: true)
            sender.activate(true)
        } else {
            dismiss()
            handler!(sender.text())
        }
    }
    
    func textChange(_ sender:TextFieldContainer, inRange: NSRange, string:String) -> Bool {
        return true
    }
    
    override func show() {
        super.show()
        inputField.activate(true)
    }
    
    func showInView(_ view:UIView) {
        superView = view
        show()
        inputField.activate(true)
    }

}
