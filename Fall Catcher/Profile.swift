//
//  Profile.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 16.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//


import UIKit

class Insurance: NSObject {
    
    var data:[String:Any] = [:]
    
    init(_ data:[String:Any]) {
        self.data = data
    }
    
    func clientName() -> String? {
        return data["insurance_client_name"] as? String
    }

    func companyName() -> String? {
        return data["insurance_company_name"] as? String
    }

    func groupNumber() -> String? {
        return data["insurance_group_number"] as? String
    }
    
    func holdersName() -> String? {
        return data["insurance_holder_name"] as? String
    }

    func policyNumber() -> String? {
        return data["insurance_policy_number"] as? String
    }
    
    func securityNumber() -> String? {
        if let number = data["social_security_number"] as? String {
            return number
        } else {
            return data["national_provider_number"] as? String
        }
    }

    func needDualDiagnosisMentalHealthAndSubstanceAbuseTreatment() -> Bool {
        if let need = data["need_diagnosis"] as? Bool {
            return need
        } else {
            return false
        }
    }
    
    func needSubstanceAbuseTreatmentHelp() -> Bool {
        if let need = data["need_treatment"] as? Bool {
            return need
        } else {
            return false
        }
    }
    
    func primaryInsuredIsDifferent() -> Bool {
        if let different = data["primary_insurance_different"] as? Bool {
            return different
        } else {
            return false
        }
    }
    
    func state() -> String? {
        return data["state"] as? String
    }

}

class Profile: NSObject {
    
    private var userData:[String:Any] = [:]

    override init() {
        super.init()
    }
    
    class func current() -> Profile? {
        if let data = UserDefaults.standard.object(forKey: "profile") as? [String:Any] {
            let profile = Profile()
            profile.userData = data
            return profile
        } else {
            return nil
        }
    }
    
    func data() -> [String:Any] {
        return userData
    }
    
    func name() -> String? {
        return userData["name"] as? String
    }
    
    func email() -> String? {
        return userData["email"] as? String
    }
    
    func avatarUrl() -> URL? {
        if let url = userData["image"] as? String {
            return URL(string: url)
        } else {
            return nil
        }
    }
    
    func phone() -> String? {
        return userData["phone"] as? String
    }
    
    func dateOfBirth() -> Date? {
        if let dateOfBirth = userData["birth_date"] as? String {
            return utcFormatter().date(from: dateOfBirth)
        } else {
            return nil
        }
    }

    func insurance() -> Insurance? {
        if let data = userData["insurance"] as? [String:Any] {
            return Insurance(data)
        } else {
            return nil
        }
    }
    
    func role() -> String? {
        return userData["role"] as? String
    }

    func setInsurance(_ insurance:Insurance) {
        userData["insurance"] = insurance.data
    }
    
    func save(_ data:[String:Any]) {
        print(data)
        if let user = data["data"] as? [String:Any] {
            if let userID = user["id"] as? Int {
                userData["id"] = userID
            }
            if let name = user["name"] as? String {
                userData["name"] = name
            }
            if let email = user["email"] as? String {
                userData["email"] = email
            }
            if let birth_date = user["birth_date"] as? String {
                userData["birth_date"] = birth_date
            }
            if let phone = user["phone"] as? String {
                userData["phone"] = phone
            }
            if let role = user["role"] as? String {
                userData["role"] = role
            }
            if let image = user["image"] as? String {
                userData["image"] = image
            }
            if let insurance_profile = user["insurance"] as? [String:Any] {
                var insurance_data:[String:Any] = [:]
                var primary_insurance_different = insurance_profile["primary_insurance_different"] as? Bool
                if primary_insurance_different == nil {
                    primary_insurance_different = false
                }
                insurance_data["primary_insurance_different"] = primary_insurance_different!
                if !primary_insurance_different! {
                    if let n = user["name"] as? String {
                        insurance_data["insurance_client_name"] = n
                        insurance_data["insurance_holder_name"] = n
                    }
                } else {
                    if let insurance_client_name = insurance_profile["insurance_client_name"] as? String {
                        insurance_data["insurance_client_name"] = insurance_client_name
                    }
                    if let insurance_holder_name = insurance_profile["insurance_holder_name"] as? String {
                        insurance_data["insurance_holder_name"] = insurance_holder_name
                    }
                }
                if let insurance_company_name = insurance_profile["insurance_company_name"] as? String {
                    insurance_data["insurance_company_name"] = insurance_company_name
                }
                if let insurance_company_name = insurance_profile["insurance_company_name"] as? String {
                    insurance_data["insurance_company_name"] = insurance_company_name
                }
                if let insurance_group_number = insurance_profile["insurance_group_number"] as? String {
                    insurance_data["insurance_group_number"] = insurance_group_number
                }
                if let insurance_policy_number = insurance_profile["insurance_policy_number"] as? String {
                    insurance_data["insurance_policy_number"] = insurance_policy_number
                }
                if let need_diagnosis = insurance_profile["need_diagnosis"] as? Bool {
                    insurance_data["need_diagnosis"] = need_diagnosis
                }
                if let need_treatment = insurance_profile["need_treatment"] as? Bool {
                    insurance_data["need_treatment"] = need_treatment
                }
                if let state = insurance_profile["state"] as? String {
                    insurance_data["state"] = state
                }
                if let social_security_number = insurance_profile["social_security_number"] as? String {
                    insurance_data["social_security_number"] = social_security_number
                }
                if let national_provider_number = insurance_profile["national_provider_number"] as? String {
                    insurance_data["national_provider_number"] = national_provider_number
                }
                userData["insurance"] = insurance_data
            }
            UserDefaults.standard.set(userData, forKey: "profile")
            UserDefaults.standard.synchronize()
        }
    }
    
    func set(birthday:Date) {
        userData["birth_date"] = utcFormatter().string(from: birthday)
    }
    
    func set(name:String, birthday:Date, email:String, phone:String) {
        userData["name"] = name
        userData["birth_date"] = utcFormatter().string(from: birthday)
        userData["email"] = email.normalizePhone()
        userData["phone"] = phone
    }
    
    class func clear() {
        UserDefaults.standard.removeObject(forKey: "profile")
        UserDefaults.standard.synchronize()
    }
    
}
