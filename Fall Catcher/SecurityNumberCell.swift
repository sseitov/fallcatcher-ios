//
//  SecurityNumberCell.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 03.11.2017.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit

class SecurityNumberCell: UITableViewCell, TextFieldContainerDelegate {
    
    @IBOutlet weak var fieldName: UILabel!
    @IBOutlet weak var fieldValue: TextFieldContainer!

    var field:String?
    
    var value:String? {
        didSet {
            if value != nil {
                fieldValue.setText(value!)
            }
        }
    }

    var delegate:CellDelegate? {
        didSet {
            fieldValue.alignment = .left
            fieldValue.activeColor = UIColor.clear
            fieldValue.textFontColor = UIColor.black
            fieldValue.placeholderColor = UIColor.lightGray
            fieldValue.textType = .numberPad
            fieldValue.placeholder = "XXX XX XXXX"
            fieldValue.returnType = .done
            fieldValue.delegate = self
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.tap))
            self.addGestureRecognizer(tap)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @objc func tap() {
        fieldValue.activate(false)
    }

    func textDone(_ sender:TextFieldContainer, text:String?) {
        self.delegate?.fieldDidChange(self.field!, value: sender.text().normalizeNumber())
    }

    func textEndEditing(_ sender:TextFieldContainer) {
        let result = sender.text().normalizeNumber()
        if result.length() < 9 {
            sender.setText("")
            field = nil
            self.delegate?.fieldDidChange(self.field!, value: nil)
        } else {
            self.delegate?.fieldDidChange(self.field!, value: result)
        }
    }
  
    func textChange(_ sender:TextFieldContainer, inRange: NSRange, string:String) -> Bool {
        let newString = (sender.text() as NSString).replacingCharacters(in: inRange, with: string)
        let num = newString
        if num.length() > 11 {
            return false
        } else if num.length() == 11 {
            sender.setText(newString.uppercased())
            sender.activate(false)
            return false
        }
        if num.length() == 3 && string.length() > 0 {
            sender.setText("\(num.uppercased()) ")
            return false
        }
        if num.length() == 6 && string.length() > 0 {
            sender.setText("\(num.uppercased()) ")
            return false
        }
        sender.setText(num.uppercased())
        return false
    }

}
