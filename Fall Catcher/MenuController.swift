//
//  MenuController.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 13.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import AMSlideMenu
import SVProgressHUD

class MenuController: AMSlideMenuLeftTableViewController {
    
    @IBOutlet weak var accountRole: UILabel!
    @IBOutlet weak var accountView: UIImageView!
    @IBOutlet weak var accountName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        accountView.setupBorder(UIColor.clear, radius: accountView.frame.size.width/2, width: 2)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 1 : 30
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "" : "information"
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.font = UIFont.mainFont(15)
        if indexPath.section == 0 {
            switch indexPath.row {
            case 1:
                cell.textLabel?.text = "My Profile"
                cell.imageView?.image = UIImage(named: "profile")
            case 2:
                cell.textLabel?.text = "My Insurance"
                cell.imageView?.image = UIImage(named: "insurance")
            default:
                cell.textLabel?.text = "Search"
                cell.imageView?.image = UIImage(named: "search")
            }
        } else {
            switch indexPath.row {
            case 1:
                cell.textLabel?.text = "About"
                cell.imageView?.image = UIImage(named: "about")
            case 2:
                cell.textLabel?.text = "Sign Out"
                cell.imageView?.image = UIImage(named: "signOut")
            default:
                cell.textLabel?.text = "Terms of Service"
                cell.imageView?.image = UIImage(named: "terms")
            }
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 1 && indexPath.row == 2 {
            let alert = createQuestion("Are you really want to sign out?", acceptTitle: "Yes", cancelTitle: "Cancel", acceptHandler:
            {
                Profile.clear()
                self.performSegue(withIdentifier: "login", sender: nil)
            })
            alert?.show()
        } else if indexPath.section == 0 && indexPath.row != 0 && Profile.current() == nil {
            let ask = createQuestion("Do you want to get account?", acceptTitle: "Yes", cancelTitle: "Now now", acceptHandler: {
                self.performSegue(withIdentifier: "login", sender: nil)
            }, cancelHandler: {
            } )
            ask?.show()
        } else {
            super.tableView(tableView, didSelectRowAt: indexPath)
        }
    }
    
    @IBAction func unwindToMenu(_ segue: UIStoryboardSegue) {
        performSegue(withIdentifier: "search", sender: nil)
    }

    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "login" {
        }
    }
    
}
