//
//  RestoreController.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 28.09.2017.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol RestoreControllerDelegate {
    func passwordRestored()
}

class RestoreController: UIViewController, TextFieldContainerDelegate {

    var request:[String:String]?
    var delegate:RestoreControllerDelegate?
    
    @IBOutlet weak var codeView: TextFieldContainer!
    @IBOutlet weak var passwordView: TextFieldContainer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        codeView.textFontColor = UIColor.white
        codeView.textType = .asciiCapable
        codeView.placeholder = "Code"
        codeView.returnType = .done
        codeView.delegate = self
        
        passwordView.textFontColor = UIColor.white
        passwordView.placeholder = "New Password"
        passwordView.returnType = .go
        passwordView.secure = true
        passwordView.delegate = self
    }
    
    @IBAction func back(_ sender: Any) {
        goBack()
    }
    
    // MARK: - TextFieldContainer Delegate
    
    func textDone(_ sender:TextFieldContainer, text:String?) {
        if sender == passwordView {
            if let code = request!["code"], code == codeView.text(), let email = request!["email"] {
                SVProgressHUD.show()
                FallCatcherAPI.shared.newPassword(email:email, code:code, password:passwordView.text(), error: { err in
                    SVProgressHUD.dismiss()
                    if err != nil {
                        self.showMessage(err!.localizedDescription, messageType: .error)
                    } else {
                        self.showMessage("Password reset successful", messageType: .information, messageHandler: {
                            if self.delegate != nil {
                                self.delegate?.passwordRestored()
                            } else {
                                self.goBack()
                            }
                        })
                    }
                })
            } else {
                showMessage("Invalid code. Check your mailbox", messageType: .error, messageHandler: {
                    self.codeView.activate(true)
                })
            }
        }
    }
    
    func textChange(_ sender:TextFieldContainer, inRange: NSRange, string:String) -> Bool {
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
