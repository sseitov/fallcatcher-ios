//
//  UnlockController.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 25.08.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage
import LocalAuthentication

class UnlockController: UIViewController, TextFieldContainerDelegate, RestoreControllerDelegate {

    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var password: TextFieldContainer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let user = Profile.current()?.name() {
            name.text = "Hello, \(user)"
        } else {
            name.text = ""
        }
        if let url = Profile.current()?.avatarUrl() {
            if let image = SDImageCache.shared().imageFromCache(forKey: url.absoluteString) {
                self.avatar.image = image.withSize(self.avatar.frame.size).inCircle()
            } else {
                SDWebImageDownloader.shared().downloadImage(
                    with: url, options: [], progress: nil,
                    completed: { newImage, _, _, _ in
                        if newImage != nil {
                            SDImageCache.shared().store(newImage, forKey: url.absoluteString, completion: {
                                self.avatar.image = newImage!.withSize(self.avatar.frame.size).inCircle()
                            })
                        }
                })
            }
        }

        if checkTouchID() {
            passwordView.alpha = 0
        } else {
            passwordView.alpha = 1
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.tap))
            self.view.addGestureRecognizer(tap)
        }
    
        password.textFontColor = UIColor.white
        password.placeholder = "Password"
        password.returnType = .go
        password.secure = true
        password.delegate = self
    }
    
    @objc func tap() {
        TextFieldContainer.deactivateAll()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if checkTouchID() {
            fingerprint()
        }
    }
    
    // MARK: - TextFieldContainer Delegate
    
    func textDone(_ sender:TextFieldContainer, text:String?) {
        if password.text().isEmpty {
            showMessage("Password field required.", messageType: .error, messageHandler: {
                self.password.activate(true)
            })
        } else {
            SVProgressHUD.show(withStatus: "Login...")
            if let email = Profile.current()?.email() {
                FallCatcherAPI.shared.signIn(login: email, password: password.text(), error: { err in
                    SVProgressHUD.dismiss()
                    if err != nil {
                        self.showMessage("Invalid password.", messageType: .error)
                    } else {
                        AppUnlocked = true
                        self.dismiss(animated: true, completion: nil)
                    }
                })
            }
        }
    }
    
    func textChange(_ sender:TextFieldContainer, inRange: NSRange, string:String) -> Bool {
        return true
    }

    func fingerprint() {
        let authContext = LAContext()
        authContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Put your finger on Home button!", reply: { (success, error) -> Void in
            DispatchQueue.main.async { () -> Void in
                if( success ) {
                    AppUnlocked = true
                    self.dismiss(animated: true, completion: nil)
                } else {
                    // Check if there is an error
                    if let laError = error as? LAError {
                        if laError.code == .userFallback {
                            self.password.activate(true)
                        } else if laError.code != .userCancel && laError.code != .systemCancel {
                            DispatchQueue.main.async { () -> Void in
                                self.showMessage(self.errorMessageForLAErrorCode(laError.code), messageType: .error)
                            }
                        } else {
                            UIView.animate(withDuration: 0.4, animations: {
                                self.passwordView.alpha = 1
                            })
                        }
                    }
                }
            }
        })
    }
    
    // MARK: - TouchID utility
    
    func checkTouchID() -> Bool {
        let authenticationContext = LAContext()
        var error:NSError?
        return authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error)
    }
    
    func errorMessageForLAErrorCode(_ errorCode:LAError.Code ) -> String {
        if #available(iOS 9.0, *) {
            switch errorCode {
            case .appCancel:
                return "Authentication was cancelled by application"
            case .authenticationFailed:
                return "The user failed to provide valid credentials"
            case .invalidContext:
                return "The context is invalid"
            case .passcodeNotSet:
                return "Passcode is not set on the device"
            case .systemCancel:
                return "Authentication was cancelled by the system"
            case .touchIDLockout:
                return "Too many failed attempts."
            case .touchIDNotAvailable:
                return "TouchID is not available on the device"
            case .userCancel:
                return "The user did cancel"
            case .userFallback:
                return "The user chose to use the fallback"
            default:
                return "Did not find error code on LAError object"
            }
        } else {
            return "Autorization error"
        }
    }

    @IBAction func forgotPassword(_ sender: Any) {
        if let email = Profile.current()?.email() {
            SVProgressHUD.show()
            FallCatcherAPI.shared.resetPassword(email, error: { result, err in
                SVProgressHUD.dismiss()
                if err != nil {
                    self.showMessage(err!.localizedDescription, messageType: .error)
                } else {
                    self.performSegue(withIdentifier: "restorePassword", sender: result)
                }
            })
        }
    }
    
    func passwordRestored() {
        AppUnlocked = true
        dismiss(animated: true, completion: nil)
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "restorePassword" {
            let next = segue.destination as! RestoreController
            next.request = sender as? [String:String]
            next.delegate = self
        }
    }
    

}
