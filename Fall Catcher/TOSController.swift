//
//  TOSController.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 18.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import SVProgressHUD

class TOSController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var textView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTitle("Terms of Service")
        
        if let path = Bundle.main.path(forResource: "policy", ofType: "docx") {
            let url = URL(fileURLWithPath: path)
            let request = URLRequest(url: url)
            textView.loadRequest(request)
        }
    }

    deinit {
        SVProgressHUD.dismiss()
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        SVProgressHUD.show()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
    }
}
