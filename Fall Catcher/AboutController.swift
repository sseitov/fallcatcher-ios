//
//  AboutController.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 18.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit

class AboutController: UIViewController {

    @IBOutlet weak var version: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTitle("About")
        
        let versionNumber: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
//        let buildNumber: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
        version.text = "Version \(versionNumber)" // (\(buildNumber))"
    }

}
