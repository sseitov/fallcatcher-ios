//
//  ProfileController.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 18.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD

class ProfileController: UIViewController, TextFieldContainerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var avatarButton: UIButton!
    @IBOutlet weak var name: TextFieldContainer!
    @IBOutlet weak var birthdayButton: UIButton!
    @IBOutlet weak var email: TextFieldContainer!
    @IBOutlet weak var phone: TextFieldContainer!
    
    private var avatar:UIImage?
    private var birthday:Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTitle("My Profile")
        
        avatarButton.setupBorder(UIColor.clear, radius: avatarButton.frame.size.width/2, width: 2)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tap))
        self.view.addGestureRecognizer(tap)

        name.activeColor = UIColor.clear
        name.textFontColor = UIColor.white
        name.autocapitalizationType = .words
        name.placeholder = "Full Name"
        name.returnType = .done
        name.delegate = self
        
        email.activeColor = UIColor.clear
        email.textFontColor = UIColor.white
        email.textType = .emailAddress
        email.placeholder = "E-Mail"
        email.returnType = .done
        email.delegate = self
        
        phone.activeColor = UIColor.clear
        phone.textFontColor = UIColor.white
        phone.textType = .phonePad
        phone.placeholder = "(000) 000 0000"
        phone.returnType = .done
        phone.delegate = self
        
        if let profile = Profile.current() {
            if let _name = profile.name() {
                name.setText(_name)
            }
            if let _email = profile.email() {
                email.setText(_email)
            }
            if let _phone = profile.phone() {
                phone.setText(_phone.normalizePhone().phoneText())
            }
            birthday = profile.dateOfBirth()
            if birthday != nil {
                birthdayButton.setTitle(textYearFormatter().string(from: birthday!), for: .normal)
            }
            if let url = profile.avatarUrl() {
                if let image = SDImageCache.shared().imageFromCache(forKey: url.absoluteString) {
                    self.avatarButton.setImage(image.withSize(self.avatarButton.frame.size).inCircle(), for: .normal)
                } else {
                    SDWebImageDownloader.shared().downloadImage(
                        with: url, options: [], progress: nil,
                        completed: { newImage, _, _, _ in
                            if newImage != nil {
                                SDImageCache.shared().store(newImage, forKey: url.absoluteString, completion: {
                                    self.avatarButton.setImage(newImage!.withSize(self.avatarButton.frame.size).inCircle(), for: .normal)
                                })
                            }
                    })
                }
            } else {
                self.avatarButton.setImage(UIImage(named:"avatar"), for: .normal)
            }
        }
    }

    @objc func tap() {
        TextFieldContainer.deactivateAll()
    }
    
    // MARK: - TextFieldContainer Delegate
    
    func textDone(_ sender:TextFieldContainer, text:String?) {
    }

    func textStartEditing(_ sender:TextFieldContainer) {
        if (sender == phone) {
            sender.setText("")
        }
    }

    func textEndEditing(_ sender:TextFieldContainer) {
        if (sender == phone) {
            if sender.text().normalizePhone().length() < 10 {
                sender.setText("")
            }
        }
    }
    
    func textChange(_ sender:TextFieldContainer, inRange: NSRange, string:String) -> Bool {
        if (sender == phone) {
            let newString = (sender.text() as NSString).replacingCharacters(in: inRange, with: string)
            let num = newString.normalizePhone()
            if num.length() > 10 {
                return false
            } else if num.length() == 10 {
                sender.setText(newString)
                sender.activate(false)
                return false
            }
            if num.length() == 3 && string.length() > 0 {
                sender.setText("(\(num)) ")
                return false
            }
            if num.length() == 6 && string.length() > 0 {
                let index = num.index(num.startIndex, offsetBy: 3)
                let code = num[..<index]
                let num = num[index...]
                sender.setText("(\(code)) \(num)-")
                return false
            }
            return true
        } else {
            return true
        }
    }
    
    // MARK: - UIImagePickerController delegate
    
    @IBAction func inputAvatar(_ sender: UIButton) {
        UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder), to: nil, from: nil, for: nil)
        let actionView = ActionSheet.create(
            title: "Choose Photo",
            actions: ["From Camera Roll", "Use Camera"],
            handler1: {
                let imagePicker = UIImagePickerController()
                imagePicker.allowsEditing = false
                imagePicker.sourceType = .photoLibrary
                imagePicker.delegate = self
                imagePicker.modalPresentationStyle = .formSheet
                if let font = UIFont(name: "HelveticaNeue-CondensedBold", size: 15) {
                    imagePicker.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.mainColor(), NSAttributedStringKey.font : font]
                }
                imagePicker.navigationBar.tintColor = UIColor.mainColor()
                self.present(imagePicker, animated: true, completion: nil)
        }, handler2: {
            let imagePicker = UIImagePickerController()
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        })
        actionView?.show()
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: {
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                self.avatar = pickedImage.withSize(CGSize(width:300, height:300))
                self.avatarButton.setImage(self.avatar!.inCircle(), for: .normal)
            }
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func inputDate(_ sender: UIButton) {
        let alert = DatePicker.create(initDate: birthday, acceptHandler: { date in
            self.birthday = date
            sender.setTitle(textYearFormatter().string(from: date!), for: .normal)
        })
        alert?.show()
    }
  
    @IBAction func save(_ sender: Any) {
        if let user = Profile.current() {
            if birthday != nil {
                saveUserData(user)
            } else {
                showMessage("You must provide your bithday.", messageType: .information, messageHandler: {
                    let alert = DatePicker.create(initDate: self.birthday, acceptHandler: { date in
                        self.birthday = date
                        self.birthdayButton.setTitle(textYearFormatter().string(from: date!), for: .normal)
                        self.saveUserData(user)
                    })
                    alert?.show()
                })
            }
        }
    }
    
    private func saveUserData(_ user:Profile) {
        user.set(name: name.text(), birthday: birthday!, email: email.text(), phone: phone.text().normalizePhone())
        SVProgressHUD.show(withStatus: "Save...")
        FallCatcherAPI.shared.upload(user, image: avatar, error: { err in
            SVProgressHUD.dismiss()
            if err != nil {
                print(err!.localizedDescription)
                self.showMessage("Error save profile form.", messageType: .error)
            } else {
                self.showMessage("Data was saved.", messageType: .information, messageHandler: {
                    SDImageCache.shared().clearMemory()
                    SDImageCache.shared().clearDisk(onCompletion: {
                        SVProgressHUD.dismiss()
                    })
                })
            }
        })
    }
}
