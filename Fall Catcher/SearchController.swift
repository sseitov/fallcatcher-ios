//
//  SearchController.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 13.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import GoogleMaps
import SVProgressHUD

fileprivate let DEFAULT_MAP_ZOOM:Float = 10
fileprivate let DEFAULT_MAP_ZOOM_NO_LOCATION:Float = 3
fileprivate let DEFAULT_MAP_LOCATION =  CLLocationCoordinate2D(latitude: 40.424012, longitude: -98.811832)

class PlaceMarker : GMSMarker {
    var place:Place?
    
    init(_ place:Place) {
        self.place = place
    }
}

class SearchController: UIViewController, GMSMapViewDelegate, SearchTableDelegate, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate  {

    @IBOutlet weak var map: GMSMapView!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var pointer: UIImageView!
    
    private var markers:[PlaceMarker] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTitle("Search")
        tableHeight.constant = 0
        table.setupBorder(UIColor.mainColor(), radius: 2, width: 2)
        map.delegate = self
        map.moveCamera(GMSCameraUpdate.setTarget(DEFAULT_MAP_LOCATION, zoom: DEFAULT_MAP_ZOOM_NO_LOCATION))
        map.isMyLocationEnabled = true
        map.hideGMSMapViewButton()
        pointer.isHidden = true
        
        let tap = UIPanGestureRecognizer(target: self, action: #selector(self.mapPan(recognizer:)))
        tap.delegate = self
        tap.maximumNumberOfTouches = 1
        tap.minimumNumberOfTouches = 1
        map.addGestureRecognizer(tap)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        map.bringSubview(toFront: pointer)
        
        if !AppUnlocked {
            if Profile.current() == nil {
                performSegue(withIdentifier: "login", sender: nil)
            } else {
                unlockScreen(self)
            }
        } else {
            if markers.count == 0 {
                searchNearBy()
            }
        }
    }
    
    @IBAction func searchNearBy() {
        SVProgressHUD.show(withStatus: "Search nearby...")
        LocationManager.shared.getCurrentLocation({ location in
            FallCatcherAPI.shared.trackLocation(location)
            SVProgressHUD.dismiss()
            self.searchByCoordinate(location.coordinate, radius: 10, moveCamera: true)
        })
    }
    
    @IBAction func search(_ sender: Any) {
        let ask = ActionSheet.create(title: "Search", actions: ["By City", "By State", "By Zip code"], handler1: {
            self.performSegue(withIdentifier: "search", sender: SearchMode.city)
        }, handler2: {
            self.performSegue(withIdentifier: "search", sender: SearchMode.state)
        }, handler3: {
            self.performSegue(withIdentifier: "search", sender: SearchMode.zip)
        })
        ask?.show()
    }
    
    func searchByCoordinate(_ coordinate:CLLocationCoordinate2D, radius:Int, moveCamera:Bool) {
        SVProgressHUD.show()
        FallCatcherAPI.shared.placesByLocation(coordinate, radius: radius, places: { centers, error in
            SVProgressHUD.dismiss()
            if error == nil {
                self.didFound(centers, moveCamera: moveCamera)
            } else {
                self.showMessage(error!.localizedDescription, messageType: .error)
            }
        })
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool
    {
        return true
    }
 
    @objc func mapPan(recognizer:UIGestureRecognizer) {
        if recognizer.state == .began {
            pointer.isHidden = false
        } else if recognizer.state == .ended {
            pointer.isHidden = true
            searchByCoordinate(map.camera.target, radius: 20, moveCamera: true)
        }
    }
    
    func clearMarkers() {
        for marker in markers {
            marker.map = nil
        }
        markers.removeAll()
    }
    
    func addMarker(_ place:Place) {
        let marker = PlaceMarker(place)
        marker.position = place.Coordinate()!
        marker.snippet = place.Name()
        marker.icon = UIImage(named: "marker")

        marker.map = map
        markers.append(marker)
    }
    
    func didFound(_ places:[Place], moveCamera:Bool) {
        clearMarkers()
        var bounds = GMSCoordinateBounds()
        for place in places {
            if let coord = place.Coordinate() {
                bounds = bounds.includingCoordinate(coord)
                addMarker(place)
            }
        }
        tableHeight.constant = markers.count > 0 ? 252 : 0
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in
            if (moveCamera) {
                let update = GMSCameraUpdate.fit(bounds, withPadding: 20)
                self.map.animate(with: update)
            }
            self.table.reloadData()
        })
    }
    
    // MARK: - GMSMapView Delegate
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let place = marker as? PlaceMarker, let index = markers.index(of: place) {
            let indexPath = IndexPath(row: index, section: 0)
            table.selectRow(at: indexPath, animated: true, scrollPosition: .top)
            return false
        } else {
            return true
        }
    }

    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        if let place = marker as? PlaceMarker {
            performSegue(withIdentifier: "showPlace", sender: place.place)
        }
    }
 /*
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        SVProgressHUD.show()
        FallCatcherAPI.shared.placesByLocation(position.target, places: { centers, error in
            SVProgressHUD.dismiss()
            if error == nil {
                self.didFound(centers)
            } else {
                self.showMessage(error!.localizedDescription, messageType: .error)
            }
        })
    }
*/
    // MARK: - UITableView DataSource

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return markers.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "place", for: indexPath) as! PlaceCell
        cell.place = markers[indexPath.row].place
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let place = markers[indexPath.row].place
        performSegue(withIdentifier: "showPlace", sender: place)
    }
    
    @IBAction func zoomOut(_ sender: Any) {
        var zoom = map.camera.zoom
        if zoom > DEFAULT_MAP_ZOOM_NO_LOCATION {
            zoom -= 1
            let position = GMSCameraPosition(target: map.camera.target, zoom: zoom, bearing: 0, viewingAngle: 0)
            map.animate(to: position)
        }
    }
    
    @IBAction func zoomIn(_ sender: Any) {
        var zoom = map.camera.zoom
        if zoom < 16 {
            zoom += 1
            let position = GMSCameraPosition(target: map.camera.target, zoom: zoom, bearing: 0, viewingAngle: 0)
            map.animate(to: position)
        }
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "search" {
            let next = segue.destination as! SearchTableController
            next.delegate = self
            if let mode = sender as? SearchMode {
                next.searchMode = mode
            }
        } else if segue.identifier == "showPlace" {
            let next = segue.destination as! PlaceController
            next.place = sender as? Place
        }
    }

}
