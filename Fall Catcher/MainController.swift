//
//  MainController.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 13.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import AMSlideMenu
import SDWebImage
import SVProgressHUD

class MainController: AMSlideMenuMainViewController {
    
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .all
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isInitialStart = false
    }
    
    override func primaryMenu() -> AMPrimaryMenu {
        return AMPrimaryMenuLeft
    }
    
    // MARK: - Left menu

    override func openLeftMenu() {
        super.openLeftMenu()
        SVProgressHUD.dismiss()
        if let menu = self.leftMenu as? MenuController {
            if let profile = Profile.current() {
                menu.accountName.text = profile.name()
                menu.accountRole.text = profile.role()
                menu.accountView.image = nil
                if let avatarURL = profile.avatarUrl() {
                    if let image = SDImageCache.shared().imageFromCache(forKey: avatarURL.absoluteString) {
                        menu.accountView.image = image.withSize(menu.accountView.frame.size).inCircle()
                    } else {
                        SDWebImageDownloader.shared().downloadImage(
                            with: profile.avatarUrl(), options: [], progress: nil,
                            completed: { image, _, _, _ in
                                if image != nil {
                                    SDImageCache.shared().store(image, forKey: avatarURL.absoluteString, completion: {
                                        menu.accountView.image = image!.withSize(menu.accountView.frame.size).inCircle()
                                    })
                                }
                        })
                    }
                } else {
                    menu.accountView.image = UIImage(named: "avatar")
                }
            } else {
                menu.accountView.image = UIImage(named: "guest")
                menu.accountName.text = "Guest Account"
                menu.accountRole.text = ""
            }
        }
    }
    
    override func initialIndexPathForLeftMenu() -> IndexPath! {
        return IndexPath(row: 0, section: 0)
    }
    
    override func segueIdentifierForIndexPath(inLeftMenu indexPath: IndexPath!) -> String! {
        if indexPath.section == 0 {
            switch indexPath.row {
            case 1:
                return "profile"
            case 2:
                return "insurance"
            default:
                return "search"
            }
        } else {
            switch indexPath.row {
            case 0:
                return "tos"
            case 1:
                return "about"
            default:
                return "login"
            }
        }
    }
    
    override func configureLeftMenuButton(_ button: UIButton!) {
        button.frame = CGRect(x: 0, y: 0, width: 25, height: 13)
        button.backgroundColor = UIColor.clear
        button.setImage(UIImage(named: "menuButton"), for: .normal)
    }
    
    override func leftMenuWidth() -> CGFloat {
        return 260
    }
    
    override func deepnessForLeftMenu() -> Bool {
        return true
    }
    
    override func maxDarknessWhileLeftMenu() -> CGFloat {
        return 0.5
    }
    
    // MARK: - Common
    
    override func configureSlideLayer(_ layer: CALayer!) {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowRadius = 5
        layer.masksToBounds = false
        layer.shadowPath = UIBezierPath(rect: self.view.layer.bounds).cgPath
    }
    
    override func openAnimationCurve() -> UIViewAnimationOptions {
        return .curveEaseOut
    }
    
    override func closeAnimationCurve() -> UIViewAnimationOptions {
        return .curveEaseOut
    }
    
}
