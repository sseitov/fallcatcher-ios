//
//  Extensions.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 13.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit

// MARK: - String extension

func utcFormatter() -> DateFormatter {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    formatter.timeZone = TimeZone(abbreviation: "UTC")
    return formatter
}

func textDateFormatter() -> DateFormatter {
    let formatter = DateFormatter()
    formatter.dateStyle = .short
    formatter.timeStyle = .short
    formatter.doesRelativeDateFormatting = true
    return formatter
}

func textYearFormatter() -> DateFormatter {
    let formatter = DateFormatter()
    formatter.dateStyle = .long
    formatter.timeStyle = .none
    return formatter
}

extension Int
{
    static func random(_ range: CountableClosedRange<Int> ) -> Int
    {
        var offset = 0
        
        if range.lowerBound < 0   // allow negative ranges
        {
            offset = abs(range.lowerBound)
        }
        
        let mini = UInt32(range.lowerBound + offset)
        let maxi = UInt32(range.upperBound   + offset)
        
        return Int(mini + arc4random_uniform(maxi - mini)) - offset
    }
}

extension String {
    
    func isEmail() -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
    }
    
    func length() -> Int {
        return (self as NSString).length
    }
    
    func normalizeNumber() -> String {
        let filtered = self.filter({ char in
            if char == " " {
                return false
            } else if char == "-" {
                return false
            }
            return true
        })
        return filtered
    }
    
    func numberText() -> String {
        var text = self
        var index = self.index(text.startIndex, offsetBy: 3)
        text.insert(" ", at: index)
        index = self.index(text.startIndex, offsetBy: 6)
        text.insert(" ", at: index)
        return text
    }

    func normalizePhone() -> String {
        let filtered = self.filter({ char in
            if char == " " {
                return false
            } else if char == "(" {
                return false
            } else if char == ")" {
                return false
            } else if char == "-" {
                return false
            }
            return true
        })
        return filtered
    }
    
    func removeSpaces() -> String {
        let filtered = self.filter({ char in
            return (char != " ")
        })
        return String(filtered)
    }
    
    func phoneText() -> String {
        let index = self.index(self.startIndex, offsetBy: 3)
        let code = self[..<index]
        let num = self[index...]
        var text = "(\(code)) \(num)"
        let ind = text.index(text.endIndex, offsetBy: -4)
        text.insert("-", at: ind)
        return text
    }
        
    func digitsFromString() -> String {
        let digitSet = CharacterSet.decimalDigits
        let filteredCharacters = self.characters.filter {
            return  String($0).rangeOfCharacter(from: digitSet) != nil
        }
        return String(filteredCharacters)
    }

}

// MARK: - UIFont extension

extension UIFont {
    
    class func mainFont(_ size:CGFloat = 17) -> UIFont {
        return UIFont(name: "HelveticaNeue", size: size)!
    }
    
    class func thinFont(_ size:CGFloat = 17) -> UIFont {
        return UIFont(name: "HelveticaNeue-Thin", size: size)!
    }
    
    class func condensedFont(_ size:CGFloat = 17) -> UIFont {
        return UIFont(name: "HelveticaNeue-CondensedBold", size: size)!
    }
}

// MARK: - UIColor extension

extension UIColor {
    class func color(_ r: Float, _ g: Float, _ b: Float, _ a: Float) -> UIColor {
        return UIColor(red: CGFloat(r/255.0), green: CGFloat(g/255.0), blue: CGFloat(b/255.0), alpha: CGFloat(a))
    }
    
    class func color(_ rgb:UInt32) -> UIColor {
        let red = CGFloat((rgb & 0xFF0000) >> 16)
        let green = CGFloat((rgb & 0xFF00) >> 8)
        let blue = CGFloat(rgb & 0xFF)
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: 1.0)
    }
    
    class func mainColor() -> UIColor {
        return color(249, 93, 81, 1)
    }
    
    class func mainColor(_ alpha:Float) -> UIColor {
        return color(249, 93, 81, alpha)
    }
    
    class func errorColor() -> UIColor {
        return color(255, 0, 0, 1)
    }
    
    class func nonActiveColor() -> UIColor {
        return color(60, 174, 209, 1)
    }
    
    class func emailColor() -> UIColor {
        return color(250, 255, 192, 1)
    }

}

// MARK: - UIImage extension

extension UIImage {
    class func imageWithColor(_ color: UIColor, size: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(size)
        color.setFill()
        UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    func withAlpha(_ alpha: CGFloat) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(at: CGPoint.zero, blendMode: .normal, alpha: alpha)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func withSize(_ newSize:CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        let aspect = self.size.width / self.size.height
        var width = newSize.width
        var height = newSize.height
        if aspect > 1 { // landscape
            width = aspect*height
        } else if aspect < 1 {
            height = width / aspect
        }
        self.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func inCircle() -> UIImage {
        let newImage = self.copy() as! UIImage
        let cornerRadius = self.size.height/2
        UIGraphicsBeginImageContextWithOptions(self.size, false, 1.0)
        let bounds = CGRect(origin: CGPoint(), size: self.size)
        UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).addClip()
        newImage.draw(in: bounds)
        let finalImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return finalImage!
    }
}

// MARK: - UIView extension

extension UIView {
    
    func setupBorder(_ color:UIColor, radius:CGFloat, width:CGFloat = 1) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    
    func hideGMSMapViewButton() {
        for v in subviews {
            v.hideGMSMapViewButton()
            if v is UIButton {
                v.isHidden = true
            }
        }
    }
}

// MARK: - UIViewController extension

enum MessageType {
    case error, success, information
}

extension UIViewController {
    
    func setupTitle(_ text:String) {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 44))
        label.textAlignment = .center
        label.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 15)
        label.text = text
        label.textColor = UIColor.white
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        navigationItem.titleView = label
    }
    
    func setupBackButton() {
        navigationItem.leftBarButtonItem?.target = self
        navigationItem.leftBarButtonItem?.action = #selector(self.goBack)
    }
    
    @objc func goBack() {
        _ = self.navigationController!.popViewController(animated: true)
    }
    
    // MARK: - alerts
    
    func showMessage(_ error:String, messageType:MessageType, messageHandler: (() -> ())? = nil) {
        var title:String = ""
        switch messageType {
        case .success:
            title = NSLocalizedString("Success", comment: "")
        case .information:
            title = NSLocalizedString("Information", comment: "")
        default:
            title = NSLocalizedString("Error", comment: "")
        }
        let alert = LGAlertView.decoratedAlert(withTitle:title.uppercased(), message: error, cancelButtonTitle: "OK", cancelButtonBlock: { alert in
            if messageHandler != nil {
                messageHandler!()
            }
        })
        alert!.titleLabel.textColor = messageType == .error ? UIColor.errorColor() : UIColor.mainColor()
        alert?.okButton.setupBorder(UIColor.clear, radius: 10)
        alert?.containerView.setupBorder(UIColor.clear, radius: 15)
        alert?.show()
    }
    
    func createQuestion(_ question:String, acceptTitle:String, cancelTitle:String, acceptHandler:@escaping () -> (), cancelHandler: (() -> ())? = nil) -> LGAlertView? {
        
        let alert = LGAlertView.alert(
            withTitle: "",
            message: question,
            cancelButtonTitle: cancelTitle,
            otherButtonTitle: acceptTitle,
            cancelButtonBlock: { alert in
                if cancelHandler != nil {
                    cancelHandler!()
                }
        },
            otherButtonBlock: { alert in
                alert?.dismiss()
                acceptHandler()
        })
        alert?.cancelButton.setupBorder(UIColor.clear, radius: 10)
        alert?.otherButton.setupBorder(UIColor.clear, radius: 10)
        alert?.containerView.setupBorder(UIColor.clear, radius: 15)
        return alert
    }
}

