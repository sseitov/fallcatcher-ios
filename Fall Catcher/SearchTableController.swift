//
//  SearchTableController.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 13.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol SearchTableDelegate {
    func didFound(_ places:[Place], moveCamera:Bool)
}

enum SearchMode {
    case city
    case state
    case zip
}

class SearchTableController: UITableViewController, UISearchBarDelegate {

    var delegate:SearchTableDelegate?
    var searchMode:SearchMode = .zip
    
    private var searchBar = UISearchBar()
    private var zips:[ZipCode] = []
    private var regions:[Any] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        setupBackButton()
        
        searchBar.delegate = self
        searchBar.returnKeyType = .done
        navigationItem.titleView = searchBar
        switch searchMode {
        case .city:
            searchBar.placeholder = "city"
            searchBar.keyboardType = .asciiCapable
            searchBar.autocapitalizationType = .words
        case .state:
            searchBar.placeholder = "state"
            searchBar.keyboardType = .asciiCapable
        default:
            searchBar.placeholder = "zip code"
            searchBar.keyboardType = .numbersAndPunctuation
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchBar.becomeFirstResponder()
    }

    override func goBack() {
        TextFieldContainer.deactivateAll()
        super.goBack()
    }
    
    // MARK: - UISearchBar Delegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchBar.text != nil && !searchBar.text!.isEmpty {
            switch searchMode {
            case .city:
                regions = ZipCodes.shared.cities(searchBar.text!)
            case .state:
                regions = ZipCodes.shared.states(searchBar.text!)
            default:
                zips = ZipCodes.shared.zipCodes(searchBar.text!)
            }
        } else {
            zips = []
            regions = []
        }
        self.tableView.reloadData()
    }
    

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchMode == .zip {
            return zips.count
        } else {
            return regions.count
        }
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.font = UIFont.mainFont()
        switch searchMode {
        case .city:
            if let item = regions[indexPath.row] as? [String:String] {
                cell.textLabel?.text = item["city"]
                cell.detailTextLabel?.text = item["full_state"]
            }
        case .state:
            if let item = regions[indexPath.row] as? [String:String] {
                cell.textLabel?.text = item["full_state"]
            }
        default:
            let zip = zips[indexPath.row]
            cell.textLabel?.text = "\(zip.city!), \(zip.full_state!)"
            cell.detailTextLabel?.text = zip.zip
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SVProgressHUD.show(withStatus: "Find...")
        
        if searchMode == .zip {
            let zip = zips[indexPath.row]
            FallCatcherAPI.shared.placesByZip(zip.zip!, places: { centers, error in
                SVProgressHUD.dismiss()
                if error != nil {
                    self.showMessage(error!.localizedDescription, messageType: .error)
                } else {
                    if centers.count > 0 {
                        self.delegate?.didFound(centers, moveCamera: true)
                        self.goBack()
                    } else {
                        self.showMessage("No results.", messageType: .information)
                    }
                }
            })
        } else {
            if let city = regions[indexPath.row] as? [String:String] {
                FallCatcherAPI.shared.placesByCity(city, searchMode: searchMode, places: { centers, error in
                    SVProgressHUD.dismiss()
                    if error != nil {
                        self.showMessage(error!.localizedDescription, messageType: .error)
                    } else {
                        if centers.count > 0 {
                            self.delegate?.didFound(centers, moveCamera: true)
                            self.goBack()
                        } else {
                            self.showMessage("No results.", messageType: .information)
                        }
                    }
                })
            }
        }
 
    }
}
