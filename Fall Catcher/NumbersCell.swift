//
//  NumbersCell.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 21.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit

class NumbersCell: UITableViewCell, TextFieldContainerDelegate {

    @IBOutlet weak var fieldValue1: TextFieldContainer!
    @IBOutlet weak var fieldValue2: TextFieldContainer!

    var field1:String?
    var field2:String?

    var delegate:CellDelegate? {
        didSet {
            fieldValue1.alignment = .left
            fieldValue1.activeColor = UIColor.clear
            fieldValue1.textFontColor = UIColor.black
            fieldValue1.placeholderColor = UIColor.lightGray
            fieldValue1.textType = .numbersAndPunctuation
            fieldValue1.placeholder = "xxxxxx"
            fieldValue1.returnType = .done
            fieldValue1.delegate = self
            
            fieldValue2.alignment = .left
            fieldValue2.activeColor = UIColor.clear
            fieldValue2.textFontColor = UIColor.black
            fieldValue2.placeholderColor = UIColor.lightGray
            fieldValue2.textType = .numbersAndPunctuation
            fieldValue2.placeholder = "xxxxxx"
            fieldValue2.returnType = .done
            fieldValue2.delegate = self
        }
    }
    
    func textDone(_ sender:TextFieldContainer, text:String?) {
        if sender == fieldValue1 {
            self.delegate?.fieldDidChange(field1!, value: sender.text())
        } else {
            self.delegate?.fieldDidChange(field2!, value: sender.text())
        }
    }
    
    func textChange(_ sender:TextFieldContainer, inRange: NSRange, string:String) -> Bool {
        return true
    }
    
    func textEndEditing(_ sender:TextFieldContainer) {
        if sender == fieldValue1 {
            self.delegate?.fieldDidChange(field1!, value: sender.text())
        } else {
            self.delegate?.fieldDidChange(field2!, value: sender.text())
        }
    }

}
