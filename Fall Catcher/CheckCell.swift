//
//  CheckCell.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 21.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit

class CheckCell: UITableViewCell {

    @IBOutlet weak var checkButton: UIButton!
    
    var field:String?
    var value:Bool = false {
        didSet {
            if value {
                checkButton.setImage(UIImage(named:"checkOn"), for: .normal)
            } else {
                checkButton.setImage(UIImage(named:"checkOff"), for: .normal)
            }
        }
    }
    
    var delegate:CellDelegate?

    @IBAction func tap(_ sender: UIButton) {
        value = !value
        delegate?.fieldDidChange(field!, value: value)
    }

}
