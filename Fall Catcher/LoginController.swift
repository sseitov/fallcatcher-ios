//
//  LoginController.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 13.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import SVProgressHUD

class LoginController: UIViewController, TextFieldContainerDelegate {

    @IBOutlet weak var email: TextFieldContainer!
    @IBOutlet weak var password: TextFieldContainer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        email.textFontColor = UIColor.white
        email.textType = .emailAddress
        email.placeholder = "E-Mail"
        email.returnType = .next
        email.delegate = self
        
        password.textFontColor = UIColor.white
        password.placeholder = "Password"
        password.returnType = .go
        password.secure = true
        password.delegate = self

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tap))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func tap() {
        TextFieldContainer.deactivateAll()
    }
  
    @IBAction func signUp() {
        performSegue(withIdentifier: "signUp", sender: nil)
    }
    
    // MARK: - TextFieldContainer Delegate

    func textDone(_ sender:TextFieldContainer, text:String?) {
        if sender == email {
            if email.text().isEmail() {
                password.activate(true)
            } else {
                showMessage("Email should have xxxx@domain.prefix format.", messageType: .error, messageHandler: {
                    self.email.activate(true)
                })
            }
        } else {
            if password.text().isEmpty {
                showMessage("Password field required.", messageType: .error, messageHandler: {
                    self.password.activate(true)
                })
            } else if email.text().isEmpty {
                email.activate(true)
            } else {
                emailAuth(user: email.text(), password: password.text())
            }
        }
    }
    
    func textChange(_ sender:TextFieldContainer, inRange: NSRange, string:String) -> Bool {
        return true
    }
    
    // MARK: - email auth

    func checkAccount() -> Bool {
        if !email.text().isEmail() {
            showMessage("Email should have xxxx@domain.prefix format.", messageType: .error, messageHandler: {
                self.email.activate(true)
            })
            return false
        }
        if password.text().length() < 6 {
            showMessage("Password length can not be less then 6 symbols.", messageType: .error, messageHandler: {
                self.password.activate(true)
            })
            return false
        }
        return true
    }
    
    func emailAuth(user:String, password:String) {
        if checkAccount() {
            SVProgressHUD.show(withStatus: "Login...")
            FallCatcherAPI.shared.signIn(login: user, password: password, error: { err in
                SVProgressHUD.dismiss()
                if err != nil {
                    self.showMessage(err!.localizedDescription, messageType: .error)
                } else {
                    AppUnlocked = true
                    self.navigationController?.performSegue(withIdentifier: "unwindToMenu", sender: self)
                }
            })
        }
    }
    
    @IBAction func guestLogin(_ sender: Any) {
        AppUnlocked = true
        self.navigationController?.performSegue(withIdentifier: "unwindToMenu", sender: self)
    }
    
    @IBAction func restorePassword(_ sender: Any) {
        let alert = EmailInput.getEmail(cancelHandler: {
            
        }, acceptHandler: { email in
            self.password.setText("")
            self.email.setText(email)
            SVProgressHUD.show()
            FallCatcherAPI.shared.resetPassword(email, error: { result, err in
                SVProgressHUD.dismiss()
                if err != nil {
                    self.showMessage(err!.localizedDescription, messageType: .error)
                } else {
                    self.performSegue(withIdentifier: "restorePassword", sender: result)
                }
            })
        })
        alert?.show()
    }
    
    func passwordRestored() {
        
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "restorePassword" {
            let next = segue.destination as! RestoreController
            next.request = sender as? [String:String]
        }
    }

}
