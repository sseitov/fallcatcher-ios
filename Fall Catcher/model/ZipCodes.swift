//
//  ZipCodes.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 20.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import CoreData

class ZipCodes : NSObject {
    
    static let shared = ZipCodes()
    
    
    private override init() {
        super.init()
    }
    
    // MARK: - CoreData stack
    
    lazy var applicationDocumentsDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "ZipCodes", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("ZipCodes.sqlite")
        do {
            if !FileManager.default.fileExists(atPath: url.path) {
                let bundleUrl = Bundle.main.url(forResource: "zip_codes", withExtension: "sqlite")
                try FileManager.default.copyItem(at: bundleUrl!, to: url)
            }
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true])
        } catch {
            print("CoreData data error: \(error)")
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    func prepopulate() {
        let bundleUrl = Bundle.main.url(forResource: "zip_codes", withExtension: "sqlite")
        var db: OpaquePointer? = nil
        if sqlite3_open(bundleUrl!.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        var statement: OpaquePointer?
        if sqlite3_prepare_v2(db, "select * from zip_codes", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        
        while sqlite3_step(statement) == SQLITE_ROW {
            let zipCode = NSEntityDescription.insertNewObject(forEntityName: "ZipCode", into: managedObjectContext) as! ZipCode

            if let zip = sqlite3_column_text(statement, 0) {
                zipCode.zip = String(cString: UnsafePointer(zip))
            }
            if let state = sqlite3_column_text(statement, 1) {
                zipCode.state = String(cString: UnsafePointer(state))
            }
            if let latitude = sqlite3_column_text(statement, 2) {
                let latitudeString = String(cString: UnsafePointer(latitude))
                if let lat = Double(latitudeString.removeSpaces()) {
                    zipCode.latitude = lat
                }
            }
            if let longitude = sqlite3_column_text(statement, 3) {
                let longitudeString = String(cString: UnsafePointer(longitude))
                if let lon = Double(longitudeString.removeSpaces()) {
                    zipCode.longitude = lon
                }
            }
            if let city = sqlite3_column_text(statement, 4) {
                zipCode.city = String(cString: UnsafePointer(city))
            }
            if let full_state = sqlite3_column_text(statement, 5) {
                zipCode.full_state = String(cString: UnsafePointer(full_state))
            }
            saveContext()
        }
        
        if sqlite3_finalize(statement) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing prepared statement: \(errmsg)")
        }
        sqlite3_close(db)
    }
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                print("Saved data error: \(error)")
            }
        }
    }
    
    func zipCodesInitialized() -> Bool {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ZipCode")
        if let count = try? managedObjectContext.count(for: fetchRequest) {
            return count > 0
        } else {
            return false
        }
    }
    
    func zipCodes(_ start:String) -> [ZipCode] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ZipCode")
        fetchRequest.predicate = NSPredicate(format: "zip BEGINSWITH[c] %@", start)
        let sortDescriptor = NSSortDescriptor(key: "zip", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        if let all = try? managedObjectContext.fetch(fetchRequest) as! [ZipCode] {
            return all
        } else {
            return []
        }
    }
    
    func cities(_ start:String) -> [Any] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ZipCode")
        fetchRequest.predicate = NSPredicate(format: "city BEGINSWITH[c] %@", start)
        let sortDescriptor = NSSortDescriptor(key: "city", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.resultType = .dictionaryResultType
        fetchRequest.returnsDistinctResults = true
        fetchRequest.propertiesToFetch = ["city", "state", "full_state"]
        if let all = try? managedObjectContext.fetch(fetchRequest) {
            return all
        } else {
            return []
        }
    }
    
    func states(_ start:String) -> [Any] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ZipCode")
        fetchRequest.predicate = NSPredicate(format: "full_state BEGINSWITH[c] %@", start)
        let sortDescriptor = NSSortDescriptor(key: "full_state", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.resultType = .dictionaryResultType
        fetchRequest.returnsDistinctResults = true
        fetchRequest.propertiesToFetch = ["state", "full_state"]
        if let all = try? managedObjectContext.fetch(fetchRequest) {
            return all
        } else {
            return []
        }
    }

}
