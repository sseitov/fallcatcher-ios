//
//  ZipCode+CoreDataProperties.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 20.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import Foundation
import CoreData


extension ZipCode {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ZipCode> {
        return NSFetchRequest<ZipCode>(entityName: "ZipCode")
    }

    @NSManaged public var zip: String?
    @NSManaged public var state: String?
    @NSManaged public var longitude: Double
    @NSManaged public var latitude: Double
    @NSManaged public var full_state: String?
    @NSManaged public var city: String?

}
