//
//  TextCell.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 21.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit

class TextCell: UITableViewCell, TextFieldContainerDelegate {

    @IBOutlet weak var fieldName: UILabel!
    @IBOutlet weak var fieldValue: TextFieldContainer!
    
    var field:String?
    
    var delegate:CellDelegate? {
        didSet {
            fieldValue.alignment = .left
            fieldValue.activeColor = UIColor.clear
            fieldValue.textFontColor = UIColor.black
            fieldValue.placeholderColor = UIColor.lightGray
            fieldValue.placeholder = "Type Here"
            fieldValue.autocapitalizationType = .words
            fieldValue.returnType = .done
            fieldValue.delegate = self
        }
    }

    func textDone(_ sender:TextFieldContainer, text:String?) {
        self.delegate?.fieldDidChange(self.field!, value: sender.text())
    }
    
    func textChange(_ sender:TextFieldContainer, inRange: NSRange, string:String) -> Bool {
        return true
    }

}
