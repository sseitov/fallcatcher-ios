//
//  Place.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 20.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import CoreLocation

/*
 {
 Address = "22 W 27th St # 5";
 City = "New York City";
 Contact = "";
 Email = "";
 Latitude = "40.74463272";
 Longitude = "-73.98967743";
 "Main_Phone" = "(212) 213-6376";
 Name = "Harm Reduction Coalition";
 "Phone_2" = "";
 "Phone_3" = "";
 State = NY;
 Website = "";
 "Zip_Code" = 10001;
 }
 */

class Place: NSObject {
    private var data:[String:Any] = [:]
    
    init(_ data:[String:Any]) {
        self.data = data
    }
    
    func Address() -> String? {
        return data["address"] as? String
    }
    func City() -> String? {
        return data["city"] as? String
    }
    func Contact() -> String? {
        return data["contact"] as? String
    }
    func Email() -> String? {
        return data["email"] as? String
    }
    func Coordinate() -> CLLocationCoordinate2D? {
        if let lat = data["latitude"] as? Double, let lon = data["longitude"] as? Double {
            return CLLocationCoordinate2D(latitude: lat, longitude: lon)
        } else {
            return nil
        }
    }
    func Main_Phone() -> String? {
        if let phone = data["main_phone"] as? String {
            return phone.normalizePhone()
        } else {
            return nil
        }
    }
    func Name() -> String? {
        return data["name"] as? String
    }
    func Phone_2() -> String? {
        return data["phone_2"] as? String
    }
    func Phone_3() -> String? {
        return data["phone_3"] as? String
    }
    func State() -> String? {
        if let state = data["state"] as? String {
            if let url = Bundle.main.url(forResource: "us_states", withExtension: "plist"), let states = NSDictionary(contentsOf: url)
            {
                return states.value(forKey: state) as? String
            } else {
                return state
            }
        } else {
            return nil
        }
    }
    func Website() -> String? {
        return data["website"] as? String
    }
    func Zip_Code() -> String? {
        return data["zip_code"] as? String
    }
}
