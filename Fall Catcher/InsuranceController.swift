//
//  InsuranceController.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 18.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol CellDelegate {
    func fieldDidChange(_ field:String, value:Any?)
}

class InsuranceController: UITableViewController, CellDelegate {

    var insurance:Insurance?
    let profile = Profile.current()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTitle("My Insurance")
        insurance = profile?.insurance()
        if insurance == nil {
            var data:[String:Any] = ["primary_insurance_different" : false]
            if let name = profile?.name() {
                data["insurance_client_name"] = name
                data["insurance_holder_name"] = name
            }
            insurance = Insurance(data)
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "client" : "insurance"
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 0 ? 1 : 30
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let row = insurance!.primaryInsuredIsDifferent() ? indexPath.row - 2 : indexPath.row
        if indexPath.section == 1 && row == 2 {
            return 110
        } else {
            return 70
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "text", for: indexPath) as! TextCell
                cell.field = "insurance_client_name"
                cell.fieldName.text = "Client's Name *"
                if insurance?.clientName() != nil {
                    cell.fieldValue.setText(insurance!.clientName()!)
                }
                cell.delegate = self
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "birthday", for: indexPath) as! BirthdayCell
                cell.field = "insurance_client_birth_date"
                cell.fieldName.text = "Date of Birth"
                cell.value = profile?.dateOfBirth()
                cell.delegate = self
                return cell
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "phone", for: indexPath) as! PhoneCell
                cell.field = "insurance_client_phone"
                cell.value = profile?.phone()
                cell.fieldName.text = "Phone Number"
                cell.delegate = self
                return cell
            case 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: "securityNumber", for: indexPath) as! SecurityNumberCell
                cell.field = "social_security_number"
                cell.value = insurance?.securityNumber()?.numberText()
                if let role = profile?.role(), role == "Patient" {
                    cell.fieldName.text = "Social Security Number"
                } else {
                    cell.fieldName.text = "National Provider Number"
                }
                cell.delegate = self
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "check", for: indexPath) as! CheckCell
                cell.field = "primary_insurance_different"
                cell.value = insurance!.primaryInsuredIsDifferent()
                cell.delegate = self
                return cell
            }
        } else {
            let row = insurance!.primaryInsuredIsDifferent() ? indexPath.row - 2 : indexPath.row
            switch row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "text", for: indexPath) as! TextCell
                cell.field = "insurance_company_name"
                cell.fieldName.text = "Insurance Company Name"
                if insurance?.companyName() != nil {
                    cell.fieldValue.setText(insurance!.companyName()!)
                }
                cell.delegate = self
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "numbers", for: indexPath) as! NumbersCell
                cell.field1 = "insurance_group_number"
                if let num = insurance!.groupNumber() {
                    cell.fieldValue1.setText(num)
                }
                cell.field2 = "insurance_policy_number"
                if let num = insurance!.policyNumber() {
                    cell.fieldValue2.setText(num)
                }
                cell.delegate = self
                return cell
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "doubleCheck", for: indexPath) as! DoubleCheckCell
                cell.field1 = "need_treatment"
                cell.value1 = insurance!.needSubstanceAbuseTreatmentHelp()
                cell.field2 = "need_diagnosis"
                cell.value2 = insurance!.needDualDiagnosisMentalHealthAndSubstanceAbuseTreatment()
                cell.delegate = self
                cell.delegate = self
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "state", for: indexPath) as! StateCell
                cell.field = "state"
                cell.fieldName.text = "State that you want to get treated in"
                cell.value = insurance?.state()
                cell.delegate = self
                return cell
            }
        }
    }
    
    func fieldDidChange(_ field:String, value:Any?) {
        if field == "insurance_client_name" {
            if let val = value as? String {
                insurance!.data[field] = val
                if !insurance!.primaryInsuredIsDifferent() {
                    insurance!.data["insurance_holder_name"] = val
                }
            } else {
                insurance!.data.removeValue(forKey: field)
            }
        } else if field == "insurance_client_birth_date" {
            if let val = value as? Date {
                profile?.set(birthday: val)
            }
        } else if field == "insurance_client_phone" {
            if let val = value as? String {
                insurance!.data[field] = val.normalizePhone()
            } else {
                insurance!.data.removeValue(forKey: field)
            }
        } else if field == "social_security_number" {
            if let val = value as? String {
                if let role = profile?.role(), role == "Patient" {
                    insurance!.data["social_security_number"] = val.normalizeNumber()
                } else {
                    insurance!.data["national_provider_number"] = val.normalizeNumber()
                }
            }
        } else if field == "primary_insurance_different" {
            if let boolVal = value as? Bool {
                insurance!.data[field] = boolVal
                let anim:UITableViewRowAnimation = boolVal ? .bottom : .top
                tableView.reloadSections(IndexSet(integer: 1), with: anim)
            } else {
                insurance!.data[field] = false
            }
        } else if field == "insurance_holder_name" {
            if let val = value as? String {
                insurance!.data[field] = val
            } else {
                insurance!.data.removeValue(forKey: field)
            }
        } else if field == "insurance_holder_date_of_birth" {
        } else if field == "insurance_company_name" {
            if let val = value as? String {
                insurance!.data[field] = val
            } else {
                insurance!.data.removeValue(forKey: field)
            }
        } else if field == "insurance_group_number" {
            if let val = value as? String {
                insurance!.data[field] = val
            } else {
                insurance!.data.removeValue(forKey: field)
            }
        } else if field == "insurance_policy_number" {
            if let val = value as? String {
                insurance!.data[field] = val
            } else {
                insurance!.data.removeValue(forKey: field)
            }
        } else if field == "need_treatment" {
            if let val = value as? Bool {
                insurance!.data[field] = val
            } else {
                insurance!.data[field] = false
            }
        } else if field == "need_diagnosis" {
            if let val = value as? Bool {
                insurance!.data[field] = val
            } else {
                insurance!.data[field] = false
            }
        } else if field == "state" {
            if let val = value as? String {
                insurance!.data[field] = val
            } else {
                insurance!.data.removeValue(forKey: field)
            }
        }
    }

    func checkFields() -> Bool {
        if insurance?.clientName() == nil {
            return false
        } else if insurance?.holdersName() == nil {
            return false
        } else {
            return true
        }
    }
    
    @IBAction func save(_ sender: Any) {
        if checkFields() {
            if profile?.dateOfBirth() != nil {
                saveUserData()
            } else {
                showMessage("You must provide your bithday.", messageType: .information, messageHandler: {
                    let alert = DatePicker.create(initDate: nil, acceptHandler: { date in
                        if date != nil {
                            self.profile?.set(birthday: date!)
                            self.tableView.reloadData()
                            self.saveUserData()
                        }
                    })
                    alert?.show()
                })
            }
        }
    }
    
    private func saveUserData() {
        profile?.setInsurance(insurance!)
        SVProgressHUD.show(withStatus: "Save...")
        FallCatcherAPI.shared.upload(profile!, image: nil, error: { err in
            SVProgressHUD.dismiss()
            if err != nil {
                print(err!.localizedDescription)
                self.showMessage("Error save insurance form.", messageType: .error)
            } else {
                self.showMessage("Data was saved.", messageType: .information, messageHandler: {
                    self.tableView.reloadData()
                })
            }
        })
    }
}
