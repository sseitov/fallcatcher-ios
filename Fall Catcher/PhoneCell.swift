//
//  PhoneCell.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 21.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit

class PhoneCell: UITableViewCell, TextFieldContainerDelegate {

    @IBOutlet weak var fieldName: UILabel!
    @IBOutlet weak var fieldValue: TextFieldContainer!

    var field:String?
    
    var value:String? {
        didSet {
            if value != nil {
                fieldValue.setText(value!.phoneText())
            }
        }
    }
    
    var delegate:CellDelegate? {
        didSet {
            fieldValue.alignment = .left
            fieldValue.activeColor = UIColor.clear
            fieldValue.textFontColor = UIColor.black
            fieldValue.placeholderColor = UIColor.lightGray
            fieldValue.textType = .phonePad
            fieldValue.placeholder = "(000) 000 0000"
            fieldValue.returnType = .done
            fieldValue.delegate = self
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.tap))
            self.addGestureRecognizer(tap)
        }
    }
    
    @objc func tap() {
        fieldValue.activate(false)
    }

    func textDone(_ sender:TextFieldContainer, text:String?) {
        self.delegate?.fieldDidChange(self.field!, value: sender.text().normalizePhone())
    }
    
    func textEndEditing(_ sender:TextFieldContainer) {
        let result = sender.text().normalizePhone()
        if result.length() < 10 {
            sender.setText("")
            field = nil
            self.delegate?.fieldDidChange(self.field!, value: nil)
        } else {
            self.delegate?.fieldDidChange(self.field!, value: result)
        }
    }

    func textChange(_ sender:TextFieldContainer, inRange: NSRange, string:String) -> Bool {
        let newString = (sender.text() as NSString).replacingCharacters(in: inRange, with: string)
        let num = newString.normalizePhone()
        if num.length() > 10 {
            return false
        } else if num.length() == 10 {
            sender.setText(newString)
            sender.activate(false)
            return false
        }
        if num.length() == 3 && string.length() > 0 {
            sender.setText("(\(num)) ")
            return false
        }
        if num.length() == 6 && string.length() > 0 {
            let index = num.index(num.startIndex, offsetBy: 3)
            let code = num[..<index]
            let num = num[index...]
            sender.setText("(\(code)) \(num)-")
            return false
        }
        return true
    }

}
