//
//  PlaceCell.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 20.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit

class PlaceCell: UITableViewCell {

    var place:Place? {
        didSet {
            name.text = place!.Name()
            call.text = place!.Main_Phone()
            address.text = place!.Address()
            web.text = place!.Website()
        }
    }
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var call: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var web: UILabel!

}
