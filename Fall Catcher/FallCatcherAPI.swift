//
//  FallCatcherAPI.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 21.09.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import AFNetworking
import CoreLocation

// http://13.59.77.132/
// key: Ml6g1lIpbPIHYEsa
// secret: 9hSRFsibBjw8zvKFMFYlE6MLfTwN6X9w4zqcZBRj3J0Pj4gs

// super_user@fallcatcher.app
// super_user

// 13.59.77.132 old api
// 18.221.88.40 new api

let baseURL = "http://18.221.88.40/api/v1/"
let tokenKey = "6vqA2CbSMLlugY3Y"
let tokenSecret = "S9hQLZZhexIkxixGUovcam19b7D1ZWVLYjpCk1d2NslfYHJZ"

func fallCatcherError(_ text:String) -> Error {
    return NSError(domain: "com.fallCatcher", code: -1, userInfo: [NSLocalizedDescriptionKey:text])
}

struct FallCatcherError : Decodable {
    let exception:String
    let message:String
}

func fallCatcherError(_ err:Error) -> Error {
    let nsErr = err as NSError
    if let errData = nsErr.userInfo["com.alamofire.serialization.response.error.data"] as? Data {
        if let error = try? JSONDecoder().decode(FallCatcherError.self, from: errData) {
            print(error.message)
            return NSError(domain: "com.fallCatcher", code: -1, userInfo: [NSLocalizedDescriptionKey:error.message])
        }
    }
    return err
}

class FallCatcherAPI: NSObject {
    
    static let shared = FallCatcherAPI()
    
    private var api:AFHTTPSessionManager!
    private var geoCoder:AFHTTPSessionManager!

    private var token:String?
    
    private override init() {
        super.init()
        
        api = AFHTTPSessionManager(baseURL: URL(string: baseURL))
        api.requestSerializer = AFJSONRequestSerializer()
        api.requestSerializer.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        api.responseSerializer = AFJSONResponseSerializer()
        
        geoCoder = AFHTTPSessionManager(baseURL: URL(string: "https://maps.googleapis.com/maps/api/geocode/"))
        geoCoder.requestSerializer = AFJSONRequestSerializer()
        geoCoder.responseSerializer = AFJSONResponseSerializer()
    }

    func accessToken(_ error: @escaping(Error?) -> ()) {
        let params = ["key" : tokenKey, "secret" : tokenSecret]
        api.post("access-token", parameters: params, progress: { currentProgress in
        }, success: { task, result in
            if let response = result as? [String:String] {
                self.token = response["token"]
                if self.token != nil {
                    self.api.requestSerializer.setValue("Bearer \(self.token!)", forHTTPHeaderField: "Authorization")
                    Thread.sleep(forTimeInterval: 3)
                    error(nil)
                } else {
                    error(fallCatcherError("Can not receive token."))
                }
            } else {
                error(fallCatcherError("Can not receive token."))
            }
        }, failure: { task, err in
            error(fallCatcherError(err))
        })
    }
    
    private func checkToken(_ result: @escaping(Error?) -> ()) {
        if token == nil {
            accessToken({ err in
                result(err)
            })
        } else {
            result(nil)
        }
    }
    
    func signIn(login:String, password:String, error: @escaping(Error?) -> ()) {
        checkToken({ tokenError in
            if tokenError != nil {
                error(tokenError)
            } else {
                let params = ["email" : login, "password" : password]
                self.api.post("users/login", parameters: params, progress: { currentProgress in
                }, success: {task, result in
                    if let data = result as? [String:Any] {
                        let profile = Profile()
                        profile.save(data)
                        error(nil)
                    } else {
                        error(fallCatcherError("Can not get user data."))
                    }
                }, failure: {task, err in
                    error(fallCatcherError(err))
                })
            }
        })
    }

    func signUp(name:String, email:String, password:String, phone:String, role:String, number:String, avatar:UIImage?, error: @escaping(Error?) -> ()) {
        checkToken({ tokenError in
            if tokenError != nil {
                error(tokenError)
            } else {
                var params = ["name" : name,
                              "email" : email,
                              "password" : password,
                              "password_confirmation" : password,
                              "phone" : phone,
                              "role" : role,
                              ]
                if role == "Patient" {
                    params["social_security_number"] = number
                } else {
                    params["national_provider_number"] = number
                }
                var imageData:Data?
                if avatar != nil  {
                    imageData = UIImageJPEGRepresentation(avatar!, 0.7)
                }
                self.api.post("users/signup", parameters: params, constructingBodyWith: { formData in
                    if imageData != nil {
                        formData.appendPart(withFileData: imageData!, name: "image", fileName: "image.jpg", mimeType: "image/jpg")
                    }
                }, progress: { currentProgress in
                }, success: { task, result in
                    if let data = result as? [String:Any] {
                        let profile = Profile()
                        profile.save(data)
                        error(nil)
                    } else {
                        error(fallCatcherError("Can not get user data."))
                    }
                }, failure: { task, err in
                    error(fallCatcherError(err))
                })
            }
        })
    }
    
    func upload(_ profile:Profile, image:UIImage?, error: @escaping(Error?) -> ()) {
        print(profile.data())
        checkToken({ tokenError in
            if tokenError != nil {
                error(tokenError)
            } else {
                var params = profile.data()
                params.removeValue(forKey: "image")
                if let insurance = profile.insurance()?.data {
                    params.removeValue(forKey: "insurance")
                    for (key, value) in insurance {
                        params[key] = value
                    }
                }
                var imageData:Data?
                if image != nil  {
                    imageData = UIImageJPEGRepresentation(image!, 0.7)
                }
                self.api.post("users/save", parameters: params, constructingBodyWith: { formData in
                    if imageData != nil {
                        formData.appendPart(withFileData: imageData!, name: "image", fileName: "image.jpg", mimeType: "image/jpg")
                    }
                }, progress: { currentProgress in
                }, success: { task, result in
                    if let data = result as? [String:Any] {
                        Profile.clear()
                        let newProfile = Profile()
                        newProfile.save(data)
                        error(nil)
                    } else {
                        error(fallCatcherError("Can not get user data."))
                    }
                }, failure: { task, err in
                    error(fallCatcherError(err))
                })
            }
        })
    }
    
    func resetPassword(_ email:String, error: @escaping([String:String]?, Error?) -> ()) {
        checkToken({ tokenError in
            if tokenError != nil {
                error(nil, tokenError)
            } else {
                let params = ["email" : email]
                self.api.post("reset", parameters: params, progress: { currentProgress in
                }, success: {task, response in
                    if let result = response as? [String:Any], let code = result["code"] as? String {
                        error(["email" : email, "code" : code], nil)
                    } else {
                        error(nil, fallCatcherError("Can not get access token. Retry again over 5 sec."))
                    }
                }, failure: {task, err in
                    error(nil, fallCatcherError(err))
                })
            }
        })
    }
    
    func newPassword(email:String, code:String, password:String, error: @escaping(Error?) -> ()) {
        checkToken({ tokenError in
            if tokenError != nil {
                error(tokenError)
            } else {
                let params = ["email" : email, "code" : code, "new_password" : password]
                self.api.post("new-password", parameters: params, progress: { currentProgress in
                }, success: {task, response in
                    error(nil)
                }, failure: {task, err in
                    error(fallCatcherError(err))
                })
            }
        })
    }
    
    func placesByZip(_ zip:String, places: @escaping([Place], Error?) -> ()) {
        checkToken({ tokenError in
            if tokenError != nil {
                places([], tokenError)
            } else {
                let params = ["zip_code" : zip]
                self.api.get("centers", parameters: params, progress: { currentProgress in
                }, success: {task, response in
                    if let dict = response as? [String:Any], let result = dict["data"] as? [Any] {
                        var objects:[Place] = []
                        for item in result {
                            if let data = item as? [String:Any] {
                                let place = Place(data)
                                if place.Coordinate() != nil {
                                    objects.append(place)
                                }
                            }
                        }
                        places(objects, nil)
                    } else {
                        places([], nil)
                    }
                }, failure: {task, err in
                    places([], fallCatcherError(err))
                })
            }
        })
    }
    
    func placesByLocation(_ location:CLLocationCoordinate2D, radius:Int, places: @escaping([Place], Error?) -> ()) {
        checkToken({ tokenError in
            if tokenError != nil {
                places([], tokenError)
            } else {
                let params:[String:Any] = ["latitude" : location.latitude,
                                           "longitude" : location.longitude,
                                           "radius" : radius]
                self.api.get("centers", parameters: params, progress: { currentProgress in
                }, success: {task, response in
                    if let dict = response as? [String:Any], let result = dict["data"] as? [Any] {
                        var objects:[Place] = []
                        for item in result {
                            if let data = item as? [String:Any] {
                                let place = Place(data)
                                if place.Coordinate() != nil {
                                    objects.append(place)
                                }
                            }
                        }
                        places(objects, nil)
                    } else {
                        places([], nil)
                    }
                }, failure: {task, err in
                    places([], fallCatcherError(err))
                })
            }
        })
    }
    
    func placesByCity(_ city:[String:String], searchMode:SearchMode, places: @escaping([Place], Error?) -> ()) {
        checkToken({ tokenError in
            if tokenError != nil {
                places([], tokenError)
            } else {
                let params = searchMode == .city ? city : ["state" : city["state"]!.uppercased()]
                self.api.get("centers", parameters: params, progress: { currentProgress in
                }, success: {task, response in
                    if let dict = response as? [String:Any], let result = dict["data"] as? [Any] {
                        var objects:[Place] = []
                        for item in result {
                            if let data = item as? [String:Any] {
                                let place = Place(data)
                                if place.Coordinate() != nil {
                                    objects.append(place)
                                }
                            }
                        }
                        places(objects, nil)
                    } else {
                        places([], nil)
                    }
                }, failure: {task, err in
                    places([], fallCatcherError(err))
                })
            }
        })
    }
    
    func trackLocation(_ location:CLLocation) {
        checkToken({ tokenError in
            if tokenError == nil {
                if let number = Profile.current()?.insurance()?.securityNumber() {
                    let params:[String:Any] = ["user_id" : number, "latitude" : location.coordinate.latitude, "longitude" : location.coordinate.longitude]
                    self.api.post("patient/track-location", parameters: params, progress: { currentProgress in
                    }, success: {task, response in
                        print("success")
                    }, failure: {task, err in
                        print(err)
                    })
                }
            }
        })
    }
}

