//
//  DoubleCheckCell.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 21.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit

class DoubleCheckCell: UITableViewCell {

    @IBOutlet weak var checkButton1: UIButton!
    @IBOutlet weak var checkButton2: UIButton!
    
    var field1:String?
    var field2:String?
    var value1:Bool = false {
        didSet {
            if value1 {
                checkButton1.setImage(UIImage(named:"checkOn"), for: .normal)
            } else {
                checkButton1.setImage(UIImage(named:"checkOff"), for: .normal)
            }
        }
    }
    var value2:Bool = false {
        didSet {
            if value2 {
                checkButton2.setImage(UIImage(named:"checkOn"), for: .normal)
            } else {
                checkButton2.setImage(UIImage(named:"checkOff"), for: .normal)
            }
        }
    }
    
    var delegate:CellDelegate?
    
    @IBAction func tap1(_ sender: UIButton) {
        value1 = !value1
        delegate?.fieldDidChange(field1!, value: value1)
    }
    @IBAction func tap2(_ sender: UIButton) {
        value2 = !value2
        delegate?.fieldDidChange(field2!, value: value2)
    }

}
