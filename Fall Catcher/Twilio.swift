//
//  Twilio.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 16.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import AFNetworking

func twilioError(_ text:String) -> NSError {
    return NSError(domain: "com.vchannel.Fall-Catcher", code: -1, userInfo: [NSLocalizedDescriptionKey:text])
}

class Twilio: NSObject {
    
    static let shared = Twilio()
    
    private let twilioSID = "AC2b163ef932f17e45553ac190a3ffe9d1"        // Account SID from www.twilio.com/user/account
    private let twilioSecret = "70deffe90305987e2a4122917fbeac5c"       // Auth Token from www.twilio.com/user/account
    private let fromNumber = "+15023794971"
    private var manager:AFHTTPSessionManager!
    
    private override init() {
        super.init()
        
        manager = AFHTTPSessionManager(baseURL: URL(string: "https://api.twilio.com/2010-04-01/Accounts/\(twilioSID)/Messages"))
        manager.requestSerializer = AFHTTPRequestSerializer()
        manager.requestSerializer.setValue("application/x-www-form-urlencoded; charset=UTF-8", forHTTPHeaderField: "Content-Type")
    
        let s = "\(twilioSID):\(twilioSecret)"
        let d = s.data(using: .utf8)!
        let auth = "Basic \(d.base64EncodedString())"
        manager.requestSerializer.setValue(auth, forHTTPHeaderField: "Authorization")
        
        manager.responseSerializer = AFXMLParserResponseSerializer()
    }

    func sendSMS(_ message:String, to:String, success: @escaping(Error?) -> ()) {
        let params = ["From" : fromNumber, "To" : to, "Body" : message]
        manager.post("", parameters: params, progress: nil, success: { _, response in
            if let parser = response as? XMLParser, let result = NSDictionary(xmlParser: parser) {
                if result.object(forKey: "Message") != nil {
                    success(nil)
                } else if let exception = result.object(forKey: "RestException") as? [String:Any],
                    let errorMessage = exception["Message"] as? String {
                    success(twilioError(errorMessage))
                } else {
                    success(twilioError("Unknown error."))
                }
            } else {
                success(twilioError("Unknown error."))
            }
        }, failure: { _, error in
            print(error.localizedDescription)
            success(twilioError("Phone number is not valid."))
        })
    }
 }
