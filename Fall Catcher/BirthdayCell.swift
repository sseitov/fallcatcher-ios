//
//  BirthdayCell.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 21.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit

class BirthdayCell: UITableViewCell {
    
    @IBOutlet weak var fieldName: UILabel!
    @IBOutlet weak var fieldValue: UILabel!
    
    var field:String?
    var value:Date? {
        didSet {
            if value != nil {
                fieldValue.text = textYearFormatter().string(from: value!)
                fieldValue.textColor = UIColor.black
            } else {
                fieldValue.text = "Input Date"
                fieldValue.textColor = UIColor.lightGray
            }
        }
    }
    
    var delegate:CellDelegate? {
        didSet {
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
            fieldValue.addGestureRecognizer(tap)
        }
    }

    @objc func didTap() {
        let alert = DatePicker.create(initDate: value, acceptHandler: { date in
            self.value = date
            self.delegate?.fieldDidChange(self.field!, value: date)
        }, cancelHandler: { _ in
            self.value = nil
            self.delegate?.fieldDidChange(self.field!, value: nil)
        })
        alert?.show()
    }
}
