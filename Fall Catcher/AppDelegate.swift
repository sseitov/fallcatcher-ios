//
//  AppDelegate.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 13.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import IQKeyboardManager
import SVProgressHUD
import GoogleMaps

let GoolglePlaceAPIKey = "AIzaSyD-atQmFGuKjQTDeDKl_rvnd8CmC5OUDXI"
let GoolgleMapAPIKey = "AIzaSyCdIdwimIeX6hmGymdCEC9MbSKwsb2yujE"

var AppUnlocked:Bool = false

func unlockScreen(_ root:UIViewController?) {
    let unlock = UIStoryboard(name: "Main", bundle: nil)
    if let controller = unlock.instantiateViewController(withIdentifier: "Unlock") as? UINavigationController {
        controller.modalTransitionStyle = .flipHorizontal
        root?.present(controller, animated: true, completion: nil)
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Initialize Google Maps
        GMSServices.provideAPIKey(GoolgleMapAPIKey)
        
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setBackgroundColor(UIColor.mainColor())
        SVProgressHUD.setForegroundColor(UIColor.white)
        SVProgressHUD.setFont(UIFont.condensedFont(15))
        
        UIApplication.shared.statusBarStyle = .lightContent
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font : UIFont.condensedFont(17)], for: .normal)
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        AppUnlocked = false
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        if Profile.current() != nil {
            unlockScreen(window?.rootViewController)
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
/*
        FallCatcherAPI.shared.accessToken({ error in
            if error != nil {
                print(error!.localizedDescription)
            }
        })
 */
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }


}

