//
//  PolicyController.swift
//  Fall Catcher
//
//  Created by Сергей Сейтов on 22.08.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol PolicyControllerDelegate {
    func acceptPolicy()
}

class PolicyController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!

    var delegate:PolicyControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let path = Bundle.main.path(forResource: "policy", ofType: "docx") {
            let url = URL(fileURLWithPath: path)
            let request = URLRequest(url: url)
            webView.delegate = self
            webView.loadRequest(request)
        }
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        SVProgressHUD.show()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
    }

    @IBAction func reject(_ sender: Any) {
        SVProgressHUD.dismiss()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func accept(_ sender: Any) {
        SVProgressHUD.dismiss()
        delegate?.acceptPolicy()
    }
}
